﻿// EditUltra.cpp : 定义应用程序的入口点。
//

#include "framework.h"

#define MAX_LOADSTRING 100

struct EditUltraMainConfig		g_stEditUltraMainConfig ;

// 全局变量:
char		**g_argv = NULL ;
int		g_argc = 0 ;
HINSTANCE	g_hAppInstance = NULL ;				// 当前实例
HWND		g_hwndMainWindow = NULL ;			// 当前窗口
char		g_acAppName[100];
char		g_acWindowClassName[100];
WCHAR		g_szTitle[MAX_LOADSTRING];			// 标题栏文本
WCHAR		g_szWindowClass[MAX_LOADSTRING];		// 主窗口类名

/*
HWND		g_hwndToolBar_FileMenu = NULL ;
HWND		g_hwndToolBar_EditMenu = NULL ;
HWND		g_hwndReBar = NULL ;
int		g_nReBarHeight = 0 ;
*/
HWND		g_hwndToolBar = NULL ;
int		g_nToolBarHeight = 0 ;

char		g_acModuleFileName[ MAX_PATH ] ;
char		g_acModulePathName[ MAX_PATH ] ;

// 此代码模块中包含的函数的前向声明:
ATOM			MyRegisterClass(HINSTANCE hInstance);
BOOL			InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					 _In_opt_ HINSTANCE hPrevInstance,
					 _In_ LPWSTR	lpCmdLine,
					 _In_ int	nCmdShow)
{
	char	acCurrentDirectory[ MAX_PATH ] ;
	RECT	rectMainWindow ;

	int	nret = 0 ;

#if 0
	::MessageBox(NULL, "通过了", "通过了", MB_ICONERROR | MB_OK);
	exit(0);
#endif

#if 0
	/* test for test_SplitPathFilename */
	{
		test_SplitPathFilename();
		exit(0);
	}
#endif

#if 0
	/* test for GetApplicationByFileExt */
	{
		int nret = GetApplicationByFileExt(NULL,NULL) ;
		ConfirmErrorInfo("nret[%d]",nret);
		exit(0);


	}
#endif

#if 0
	/* test for SetApplicationByFileExt */
	{
		int nret = SetApplicationByFileExt(NULL,NULL) ;
		ConfirmErrorInfo("nret[%d]",nret);
		exit(0);


	}
#endif

	// ::MessageBoxW(NULL, lpCmdLine, NULL, MB_ICONERROR | MB_OK);

	memset( acCurrentDirectory , 0x00 , sizeof(acCurrentDirectory) );
	::GetCurrentDirectory( sizeof(acCurrentDirectory)-1 , acCurrentDirectory );
	SetLogcFile( (char*)"%s\\log\\EUX.log" , acCurrentDirectory );
	SetLogcLevel( LOGCLEVEL_NOLOG );
	DEBUGLOGC( "--- EUX Start Debugging ---" )

	UNREFERENCED_PARAMETER(hPrevInstance);
	g_argv = CommandLineToArgvA( lpCmdLine , & g_argc ) ;
#if 0
	{
		for( int i = 0 ; i < g_argc ; i++)
		{
			InfoBox( "[%d][%s]" , i , g_argv[i] );
		}
	}
#endif

	// 初始化全局字符串
	LoadStringA(hInstance, IDS_APP_TITLE, g_acAppName, 100);
	LoadStringA(hInstance, IDC_EUX, g_acWindowClassName, 100);
	LoadStringW(hInstance, IDS_APP_TITLE, g_szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_EUX, g_szWindowClass, MAX_LOADSTRING);

	{
		memset( g_acModuleFileName , 0x00 , sizeof(g_acModuleFileName) );
		::GetModuleFileName( NULL , g_acModuleFileName , sizeof(g_acModuleFileName) );
		strcpy( g_acModulePathName , g_acModuleFileName );
		char *p = strrchr( g_acModulePathName , '\\' ) ;
		if( p )
			*(p) = '\0' ;
	}

	HANDLE mutex = ::CreateMutex( NULL , TRUE , "Thank you , ualy" ) ;
	if( mutex == NULL )
	{
		ErrorBox( "创建互斥量失败[%d]" , ::GetLastError() );
		return -1;
	}

	if( GetLastError() == ERROR_ALREADY_EXISTS )
	{
		::WaitForSingleObject( mutex , INFINITE );
	}

	if( g_argc == 1 && g_argv[0] )
	{
		HWND hwnd = ::FindWindow( g_acWindowClassName , NULL ) ;
		if( hwnd )
		{
			COPYDATASTRUCT	cpd ;

			memset( & cpd , 0x00 , sizeof(COPYDATASTRUCT) );
			cpd.lpData = g_argv[0] ;
			cpd.cbData = (DWORD)strlen(g_argv[0]) ;
			::SendMessage( hwnd , WM_COPYDATA , 0 , (LPARAM)&cpd );
			::SendMessage( hwnd , WM_ACTIVATE , 0 , 0 );
			::ReleaseMutex( mutex );
			::CloseHandle( mutex );
			return 0;
		}
	}

	SetEditUltraMainConfigDefault( & g_stEditUltraMainConfig );

	// SetStyleThemeDefault( & (g_pstWindowTheme->stStyleTheme) );

	INIT_LIST_HEAD( & listRemoteFileServer );

	InitNavigateBackNextTraceList();

	// TODO: 在此处放置代码。
	LoadConfig();

	// 装载Scintilla控件
	HMODULE hmod = ::LoadLibrary(TEXT("SciLexer.DLL")) ;
	if (hmod == NULL)
	{
		::MessageBox(NULL, TEXT("不能装载Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		CloseHandle( mutex );
		return 1;
	}

	curl_global_init( CURL_GLOBAL_DEFAULT );

	// 执行应用程序初始化:
	MyRegisterClass(hInstance);
	if( ! InitInstance (hInstance, nCmdShow) )
	{
		CloseHandle( mutex );
		return FALSE;
	}

	UpdateWindowThemeMenu();
	UpdateFileTypeMenu();
	UpdateAllMenus( g_hwndMainWindow , NULL );

#if 0
	{
		char	acInputBuf[ 256 ] ;
		memset( acInputBuf , 0x00 , sizeof(acInputBuf) );
		nret = InputBox( hwndMainClient , "请输入：" , "输入窗口" , 0 , acInputBuf , sizeof(acInputBuf)-1 ) ;
		if( nret == IDOK )
		{
			::MessageBox( NULL , acInputBuf , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else if( nret == IDCANCEL )
		{
			::MessageBox( NULL , "输入窗口被取消" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else
		{
			::MessageBox( NULL , "输入窗口返回错误" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		// exit(0);
	}
#endif

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_EUX));

#if 0
char buf[1024];
memset(buf,0x00,sizeof(buf));
snprintf(buf,sizeof(buf)-1,"argc[%d]",argc);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
for(int i=0;i<argc;i++)
{
snprintf(buf,sizeof(buf)-1,"argv[%d][%s]",i,argv[i]);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
}
exit(0);
#endif
	
	if( g_argc >= 1 )
	{
		for( int i = 0 ; i < g_argc ; i++ )
		{
			if( g_argv[i][strlen(g_argv[i])-1] == '\\' )
				LocateDirectory( g_argv[i] );
			else
				OpenFilesDirectly( g_argv[i] );
		}
	}
	else if( g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == TRUE )
	{
		for( int index = 0 ; index < OPENFILES_ON_BOOT_MAXCOUNT ; index++ )
		{
			if( g_stEditUltraMainConfig.aacOpenFilesOnBoot[index][0] == '\0' )
				break;

			OpenFilesDirectly( g_stEditUltraMainConfig.aacOpenFilesOnBoot[index] );
		}
	}

	::ReleaseMutex( mutex );
	::CloseHandle( mutex );

	if( g_stEditUltraMainConfig.bCreateNewFileOnNewBoot == TRUE && TabCtrl_GetItemCount( g_hwndTabPages ) == 0 )
	{
		::PostMessage( g_hwndMainWindow , WM_COMMAND , WPARAM(IDM_FILE_NEW) , (LPARAM)0 );
	}

	::GetWindowRect( g_hwndMainWindow , & rectMainWindow );
	::PostMessage( g_hwndMainWindow , WM_SIZE , WPARAM(SIZE_MAXSHOW) , (LPARAM)MAKELONG(rectMainWindow.right-rectMainWindow.left,rectMainWindow.bottom-rectMainWindow.top) );

	if( g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval > 0 )
	{
		ReloadSymbolListOrTreeThreadHandler = _beginthread( ReloadSymbolListOrTreeThreadEntry , 0 , NULL ) ;
		if( ReloadSymbolListOrTreeThreadHandler == -1L )
		{
			ErrorBox( "创建定时刷新符号列表或符号树线程失败" );
			return 1;
		}
	}

	MSG msg;

	// 主消息循环:
	while( GetMessage( & msg , nullptr , 0 , 0 ) )
	{
		if ( !IsDialogMessage( hwndSearchFind, &msg ) && !IsDialogMessage( hwndSearchReplace, & msg ) )
		{
			nret = BeforeWndProc( & msg ) ;
			if( nret > 0 )
				continue;

			if( ! TranslateAccelerator( g_hwndMainWindow , hAccelTable , & msg ) )
			{
				TranslateMessage( & msg );

				nret = BeforeDispatchMessage( & msg ) ;
				if( nret > 0 )
					continue;

				DispatchMessage( & msg );
			}

			nret = AfterWndProc( & msg ) ;
			if( nret > 0 )
				continue;
		}
	}

	SaveMainConfigFile();
	SaveStyleThemeConfigFile();

	return (int) msg.wParam;
}

//
//  函数: MyRegisterClass()
//
//  目标: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style		= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_EDITULTRA));
	wcex.hCursor		= LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= MAKEINTRESOURCEA(IDC_EUX);
	wcex.lpszClassName	= g_acWindowClassName;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return ::RegisterClassEx(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目标: 保存实例句柄并创建主窗口
//
//   注释:
//
//		在此函数中，我们在全局变量中保存实例句柄并
//		创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HFONT		hControlFont ;
	long		lControlStyle ;
	RECT		rectClient ;

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	BOOL bret = InitCommonControlsEx( & icex ) ;
	if( bret != TRUE )
	{
		::MessageBox(NULL, TEXT("不能注册通用控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	hControlFont = ::CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font) ;

	g_hAppInstance = hInstance; // 将实例句柄存储在全局变量中
   
	g_hwndMainWindow = ::CreateWindowEx( WS_EX_ACCEPTFILES, g_acWindowClassName, g_acAppName, WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_CLIPCHILDREN, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	if (!g_hwndMainWindow)
	{
		return FALSE;
	}

#if 0
	g_hwndReBar = ::CreateWindowEx( WS_EX_TOOLWINDOW , REBARCLASSNAME , "" ,  WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|CCS_NODIVIDER|RBS_VARHEIGHT|RBS_BANDBORDERS , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;

	g_hwndToolBar_FileMenu = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS , 0 , 0 , 0 , 0 , g_hwndReBar , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList_FileMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 4 , 0 ) ;
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_C)) , 0 );
	HIMAGELIST hDisabledInmageList_FileMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 4 , 0 ) ;
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_D)) , 0 );
	TBBUTTON tbToolbar_FileMenu[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 0 , IDM_FILE_NEW , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 1 , IDM_FILE_OPEN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 2 , IDM_FILE_SAVE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 3 , IDM_FILE_SAVEAS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar_FileMenu , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar_FileMenu , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0])) , (LPARAM)&tbToolbar_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar_FileMenu , TB_AUTOSIZE , 0 , 0 );
	DWORD dwBtnSize_FileMenu = (DWORD) SendMessage( g_hwndToolBar_FileMenu , TB_GETBUTTONSIZE , 0 ,0 ) ;
	REBARBANDINFO rbBand_FileMenu = { sizeof(REBARBANDINFO) } ;
	// rbBand_FileMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_FileMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_FileMenu.fStyle = RBBS_CHILDEDGE|RBBS_GRIPPERALWAYS|RBBS_VARIABLEHEIGHT ;
	rbBand_FileMenu.lpText  = (char*)"ToolBar_FileMenu" ;
	rbBand_FileMenu.hwndChild = g_hwndToolBar_FileMenu ;
	// rbBand_FileMenu.cyChild = LOWORD(dwBtnSize_FileMenu);
	rbBand_FileMenu.cxMinChild = sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0]) * HIWORD(dwBtnSize_FileMenu) ;
	rbBand_FileMenu.cyMinChild = LOWORD(dwBtnSize_FileMenu) ;
	rbBand_FileMenu.cx = sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0]) * HIWORD(dwBtnSize_FileMenu) ;
	SendMessage( g_hwndReBar , RB_INSERTBAND , (WPARAM)-1 , (LPARAM)&rbBand_FileMenu );

	g_hwndToolBar_EditMenu = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS , 0 , 0 , 0 , 0 , g_hwndReBar , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList_EditMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 3 , 0 ) ;
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_C)) , 0 );
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_C)) , 0 );
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_C)) , 0 );
	HIMAGELIST hDisabledInmageList_EditMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 3 , 0 ) ;
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_D)) , 0 );
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_D)) , 0 );
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_D)) , 0 );
	TBBUTTON tbToolbar_EditMenu[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 1 , IDM_EDIT_CUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 2 , IDM_EDIT_COPY , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 3 , IDM_EDIT_PASTE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar_EditMenu , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar_EditMenu , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0])) , (LPARAM)&tbToolbar_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar_EditMenu , TB_AUTOSIZE , 0 , 0 );
	DWORD dwBtnSize_EditMenu = (DWORD) SendMessage( g_hwndToolBar_EditMenu , TB_GETBUTTONSIZE , 0 ,0 ) ;
	REBARBANDINFO rbBand_EditMenu = { sizeof(REBARBANDINFO) } ;
	// rbBand_EditMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_EditMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_EditMenu.fStyle = RBBS_CHILDEDGE|RBBS_GRIPPERALWAYS|RBBS_VARIABLEHEIGHT ;
	rbBand_EditMenu.lpText  = (char*)"ToolBar_EditMenu" ;
	rbBand_EditMenu.hwndChild = g_hwndToolBar_EditMenu ;
	// rbBand_EditMenu.cyChild = LOWORD(dwBtnSize_EditMenu);
	rbBand_EditMenu.cxMinChild = sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0]) * HIWORD(dwBtnSize_EditMenu) ;
	rbBand_EditMenu.cyMinChild = LOWORD(dwBtnSize_EditMenu) ;
	rbBand_EditMenu.cx = sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0]) * HIWORD(dwBtnSize_EditMenu) ;
	SendMessage( g_hwndReBar , RB_INSERTBAND , (WPARAM)-1 , (LPARAM)&rbBand_EditMenu );

	::GetClientRect( g_hwndReBar , & rectClient );
	g_nReBarHeight = rectClient.bottom - rectClient.top ;
#endif
	g_hwndToolBar = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS|TBSTYLE_FLAT , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList = ImageList_Create( 16 , 16 , ILC_COLOR32|ILC_MASK , 27 , 0 ) ;
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEALLFILES_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CLOSEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_UNDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FIND_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDPREV_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDNEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FOUNDLIST_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REPLACE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NAVIGATEBACK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FILETREE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_STYLETHEME_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_HEXEDITMODE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMOUT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMIN_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ABOUT_C)) , RGB(255,255,255) );
	HIMAGELIST hDisabledInmageList = ImageList_Create( 16 , 16 , ILC_COLOR24|ILC_MASK , 27 , 0 ) ;
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEALLFILES_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CLOSEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_UNDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FIND_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDPREV_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDNEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FOUNDLIST_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REPLACE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NAVIGATEBACK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FILETREE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_STYLETHEME_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_HEXEDITMODE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMOUT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMIN_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ABOUT_D)) , RGB(255,255,255) );
	TBBUTTON tbToolbar[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 0 , IDM_FILE_NEW , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"新建文件" } ,
		{ 1 , IDM_FILE_OPEN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"打开文件..." } ,
		{ 2 , IDM_FILE_SAVE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存文件" } ,
		{ 3 , IDM_FILE_SAVEAS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"另存为..." } ,
		{ 4 , IDM_FILE_SAVEALL , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存所有文件" } ,
		{ 5 , IDM_FILE_CLOSE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关闭文件" } ,
		{ 6 , IDM_FILE_REMOTE_FILESERVERS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"配置远程文件服务器..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 7 , IDM_EDIT_UNDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"撤销操作" } ,
		{ 8 , IDM_EDIT_REDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"重做操作" } ,
		{ 9 , IDM_EDIT_CUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"剪切文本" } ,
		{ 10 , IDM_EDIT_COPY , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"复制文本" } ,
		{ 11 , IDM_EDIT_PASTE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"粘贴文本" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 12 , IDM_SEARCH_FIND , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找文本..." } ,
		{ 13 , IDM_SEARCH_FINDPREV , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找上一个文本" } ,
		{ 14 , IDM_SEARCH_FINDNEXT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找下一个文本" } ,
		{ 15 , IDM_SEARCH_FOUNDLIST , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"列出包含关键词的行..." } ,
		{ 16 , IDM_SEARCH_REPLACE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"替换文本" } ,
		{ 17 , IDM_SEARCH_TOGGLE_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"切换书签" } ,
		{ 18 , IDM_SEARCH_GOTO_PREV_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到上一个书签" } ,
		{ 19 , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到下一个书签" } ,
		{ 20 , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"退回上一个位置" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 21 , IDM_VIEW_FILETREE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"显示/隐藏文件树" } ,
		{ 22 , IDM_VIEW_MODIFY_STYLETHEME , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"修改主题方案..." } ,
		{ 23 , IDM_VIEW_HEXEDIT_MODE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"切换十六进制编辑视图" } ,
		{ 24 , IDM_VIEW_ZOOMOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"放大视图" } ,
		{ 25 , IDM_VIEW_ZOOMIN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"缩小视图" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 26 , IDM_ABOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关于..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar)/sizeof(tbToolbar[0])) , (LPARAM)&tbToolbar );
	SendMessage( g_hwndToolBar , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList );
	SendMessage( g_hwndToolBar , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList );
	SendMessage( g_hwndToolBar , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar , TB_AUTOSIZE , 0 , 0 );

	::GetClientRect( g_hwndToolBar , & rectClient );
	g_nToolBarHeight = rectClient.bottom - rectClient.top ;

	/*
	::InvalidateRect( g_hwndReBar , NULL , TRUE );
	::UpdateWindow( g_hwndReBar );
	*/

	g_hwndStatusBar = ::CreateWindow( STATUSCLASSNAME , "" , SBS_SIZEGRIP|WS_CHILD|WS_VISIBLE , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;
	::GetClientRect( g_hwndStatusBar , & rectClient );
	g_nStatusBarHeight = rectClient.bottom - rectClient.top ;

	hwndSearchFind = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_FIND_DIALOG), g_hwndMainWindow , SearchFindWndProc ) ;
	if (!hwndSearchFind)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchFind , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WHOLEWORD , false );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_MATCHCASE , true );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WORDSTART , false );
	::CheckRadioButton( hwndSearchFind , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndEditBoxInSearchFind = GetDlgItem( hwndSearchFind , IDC_SEARCH_TEXT ) ;
	ShowWindow( hwndSearchFind , SW_HIDE );

	hwndSearchFound = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_FOUND_DIALOG), g_hwndMainWindow , SearchFoundWndProc ) ;
	if (!hwndSearchFound)
	{
		return FALSE;
	}
	hwndListBoxInSearchFound = GetDlgItem( hwndSearchFound , IDC_FOUNDLIST ) ;
	::SendMessage( hwndListBoxInSearchFound , WM_SETFONT , (WPARAM)hControlFont, 0);
	lControlStyle = ::GetWindowLong( hwndListBoxInSearchFound , GWL_STYLE ) ;
	lControlStyle |= LBS_NOTIFY|LBS_USETABSTOPS|LBS_HASSTRINGS ;
	::SetWindowLong( hwndListBoxInSearchFound , GWL_STYLE , lControlStyle );
	ShowWindow( hwndSearchFound , SW_HIDE );

	hwndSearchReplace = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_REPLACE_DIALOG), g_hwndMainWindow , SearchReplaceWndProc ) ;
	if (!hwndSearchReplace)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchReplace , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchReplace , IDC_AREATYPE_THISFILE , IDC_AREATYPE_ALLOPENEDFILES , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_WHOLEWORD , false );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_MATCHCASE , true );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_WORDSTART , false );
	::CheckRadioButton( hwndSearchReplace , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndFromEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_FROMTEXT ) ;
	hwndToEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_TOTEXT ) ;
	ShowWindow( hwndSearchReplace , SW_HIDE );

	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );

	UpdateAllWindows( g_hwndMainWindow );

	ShowWindow(g_hwndMainWindow, SW_SHOWMAXIMIZED);
	UpdateWindow(g_hwndMainWindow);

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目标: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//

int OnCreateWindow( HWND hWnd , WPARAM wParam , LPARAM lParam )
{
	BOOL	bret ;
	int	nret ;
	LSTATUS	lsret ;
	HKEY	regkey_EditUltra ;

	//// ::EnableMenuItem(::GetSubMenu(::GetMenu(hwndMainClient),3), 0, MF_DISABLED | MF_GRAYED | MF_BYCOMMAND);

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_TAB_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		::MessageBox(NULL, TEXT("不能注册TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nret = CreateFileTreeBar( hWnd ) ;
	if( nret )
		return nret;

	nret = CreateTabPages( hWnd ) ;
	if( nret )
		return nret;

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "*\\shell\\EUX" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, true);

		g_bIsEnvFilePopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, false);

		g_bIsEnvFilePopupMenuSelected = FALSE ;
	}

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, true);

		g_bIsEnvDirectoryPopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, false);

		g_bIsEnvDirectoryPopupMenuSelected = FALSE ;
	}

	return 0;
}

void UpdateAllWindows( HWND hWnd )
{
	RECT	rectAdjust ;
	RECT	rectFileTree ;

	CalcTabPagesHeight();

	AdjustTabPages();

	::SetWindowPos( g_hwndTabPages , HWND_TOP , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , SWP_SHOWWINDOW );
	memcpy( & rectAdjust , & g_rectTabPages , sizeof(RECT) );
	TabCtrl_AdjustRect( g_hwndTabPages , FALSE , & rectAdjust );
	
	CalcTabPagesHeight();

	AdjustTabPages();

	if( g_bIsFileTreeBarShow == TRUE )
	{
		::SetWindowPos ( g_hwndFileTreeBar , HWND_TOP ,  g_rectFileTreeBar.left , g_rectFileTreeBar.top , g_rectFileTreeBar.right-g_rectFileTreeBar.left , g_rectFileTreeBar.bottom-g_rectFileTreeBar.top , SWP_SHOWWINDOW );
		memcpy( & rectAdjust , & g_rectFileTreeBar , sizeof(RECT) );
		TabCtrl_AdjustRect( g_hwndFileTreeBar , FALSE , & rectAdjust );
		// ::ShowWindow( g_hwndFileTreeBar , (g_bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
		::ShowWindow( g_hwndFileTreeBar , SW_SHOW );
		::UpdateWindow( g_hwndFileTreeBar );
	}
	else
	{
		::SetWindowPos ( g_hwndFileTreeBar , 0 ,  0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		::ShowWindow( g_hwndFileTreeBar , SW_HIDE );
		::UpdateWindow( g_hwndFileTreeBar );
	}
	
	AdjustFileTreeBox( & g_rectFileTreeBar , & rectFileTree );

	if( g_bIsFileTreeBarShow == TRUE )
	{
		::SetWindowPos( g_hwndFileTree , HWND_TOP , rectFileTree.left , rectFileTree.top , rectFileTree.right-rectFileTree.left , rectFileTree.bottom-rectFileTree.top , SWP_SHOWWINDOW );
		// ::ShowWindow( g_hwndFileTree , (g_bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
		::ShowWindow( g_hwndFileTree , SW_SHOW );
		::InvalidateRect( g_hwndFileTree , NULL , TRUE );
		::UpdateWindow( g_hwndFileTree );
	}
	else
	{
		::SetWindowPos( g_hwndFileTree , 0 , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		::ShowWindow( g_hwndFileTree , SW_HIDE );
		::UpdateWindow( g_hwndFileTree );
	}

	::SetWindowPos( g_hwndTabPages , HWND_TOP , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , SWP_SHOWWINDOW );
	memcpy( & rectAdjust , & g_rectTabPages , sizeof(RECT) );
	TabCtrl_AdjustRect( g_hwndTabPages , FALSE , & rectAdjust );
	/*
	::ShowWindow( g_hwndTabPages , SW_SHOW);
	::InvalidateRect( g_hwndTabPages , NULL , TRUE );
	::UpdateWindow( g_hwndTabPages );
	*/
	
	int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( int nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		TCITEM tci ;
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		struct TabPage *pnodeTabPage = (struct TabPage *)(tci.lParam);
		if( pnodeTabPage != g_pnodeCurrentTabPage )
		{
			::SetWindowPos( pnodeTabPage->hwndScintilla , 0 , pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.top , pnodeTabPage->rectScintilla.right-pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.bottom-pnodeTabPage->rectScintilla.top , SWP_HIDEWINDOW );

			if( pnodeTabPage->hwndSymbolList )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolList , 0 , pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.top , pnodeTabPage->rectSymbolList.right-pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.bottom-pnodeTabPage->rectSymbolList.top , SWP_HIDEWINDOW );
				/*
				::ShowWindow( pnodeTabPage->hwndSymbolList , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolList , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolList );
				*/
			}

			if( pnodeTabPage->hwndSymbolTree )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolTree , 0 , pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.top , pnodeTabPage->rectSymbolTree.right-pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.bottom-pnodeTabPage->rectSymbolTree.top , SWP_HIDEWINDOW );
				/*
				::ShowWindow( pnodeTabPage->hwndSymbolTree , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolTree , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolTree );
				*/
			}

			if( pnodeTabPage->hwndQueryResultEdit )
			{
				/*
				::ShowWindow( pnodeTabPage->hwndQueryResultEdit , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultEdit );
				*/
			}

			if( pnodeTabPage->hwndQueryResultTable )
			{
				/*
				::ShowWindow( pnodeTabPage->hwndQueryResultTable , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultTable );
				*/
			}
		}
	}
	
	if( g_pnodeCurrentTabPage )
	{
		AdjustTabPageBox( g_pnodeCurrentTabPage );

		::SetWindowPos( g_pnodeCurrentTabPage->hwndScintilla , HWND_TOP , g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.top , g_pnodeCurrentTabPage->rectScintilla.right-g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.bottom-g_pnodeCurrentTabPage->rectScintilla.top , SWP_SHOWWINDOW );
		::InvalidateRect( g_pnodeCurrentTabPage->hwndScintilla , NULL , TRUE );
		::UpdateWindow( g_pnodeCurrentTabPage->hwndScintilla );

		if( g_pnodeCurrentTabPage->hwndSymbolList )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolList , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.top , g_pnodeCurrentTabPage->rectSymbolList.right-g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.bottom-g_pnodeCurrentTabPage->rectSymbolList.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolList , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolList , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolList );
		}

		if( g_pnodeCurrentTabPage->hwndSymbolTree )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolTree , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.top , g_pnodeCurrentTabPage->rectSymbolTree.right-g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.bottom-g_pnodeCurrentTabPage->rectSymbolTree.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolTree , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolTree , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolTree );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultEdit )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultEdit , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->rectQueryResultEdit.right-g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.bottom-g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->hwndQueryResultEdit?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultEdit , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultTable )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultTable , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->rectQueryResultListView.right-g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.bottom-g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->hwndQueryResultTable?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultTable , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultTable , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultTable );
		}

		::SetFocus( g_pnodeCurrentTabPage->hwndScintilla );
	}

	::UpdateWindow( g_hwndToolBar );
	// ::InvalidateRect( g_hwndReBar , NULL , TRUE );
	// ::UpdateWindow( g_hwndReBar );

	UpdateStatusBar( hWnd );

	// ::InvalidateRect( g_hwndMainWindow , NULL , TRUE );
	// ::UpdateWindow( g_hwndMainWindow );

	return;
}

void OnResizeWindow( HWND hWnd , int nWidth , int nHeight )
{
	if( g_hwndFileTreeBar == NULL || g_hwndTabPages == NULL )
		return;

	g_rectTabPages.right = g_rectTabPages.left + nWidth ;
	g_rectTabPages.bottom = g_rectTabPages.top + nHeight ;

	UpdateAllWindows( hWnd );

	return;
}
