#include "framework.h"

uintptr_t		ReloadSymbolListOrTreeThreadHandler = NULL ;

static int InitScintillaControlFold( struct TabPage *pnodeTabPage )
{
	// 启用折叠
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETPROPERTY ,(sptr_t)"fold" ,(sptr_t)"1" );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINTYPEN, MARGIN_FOLD_INDEX , SC_MARGIN_SYMBOL );//页边类型  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINMASKN, MARGIN_FOLD_INDEX , SC_MASK_FOLDERS ); //页边掩码  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINWIDTHN, MARGIN_FOLD_INDEX , (g_stEditUltraMainConfig.bBlockFoldVisiable==TRUE?16:0) ); //页边宽度  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETMARGINSENSITIVEN, MARGIN_FOLD_INDEX , TRUE ); //响应鼠标消息  

	// 折叠标签样式  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDER , SC_MARK_PIXMAP );   
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEROPEN , SC_MARK_PIXMAP );   
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEREND ,  SC_MARK_PIXMAP );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEROPENMID , SC_MARK_PIXMAP );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERMIDTAIL , SC_MARK_TCORNERCURVE );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERSUB , SC_MARK_VLINE );   
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERTAIL , SC_MARK_LCORNERCURVE );  

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDER , (sptr_t)plus_xpm );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEROPEN , (sptr_t)minus_xpm );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEREND , (sptr_t)plus_xpm );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEROPENMID , (sptr_t)minus_xpm );

	// 折叠标签颜色  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERSUB , 0xa0a0a0 );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERMIDTAIL , 0xa0a0a0 );  
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERTAIL , 0xa0a0a0 );  

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETFOLDFLAGS, 16|4, 0); //如果折叠就在折叠行的上下各画一条横线

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl ;
int InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl( struct TabPage *pnodeTabPage )
{
	int	nret = 0 ;

	HFONT hFont = ::CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 

	nret = IfSymbolReqularExp_CreateSymbolListCtl( pnodeTabPage , hFont ) ;
	if( nret )
	{
		::DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl ;
int InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl( struct TabPage *pnodeTabPage )
{
	int	nret = 0 ;

	HFONT hFont = ::CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 

	nret = CreateSymbolTreeCtl( pnodeTabPage , hFont ) ;
	if( nret )
	{
		::DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CPP ;
int InitTabPageControlsAfterLoadFile_CPP( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CS ;
int InitTabPageControlsAfterLoadFile_CS( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JAVA ;
int InitTabPageControlsAfterLoadFile_JAVA( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_GO ;
int InitTabPageControlsAfterLoadFile_GO( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SWIFT ;
int InitTabPageControlsAfterLoadFile_SWIFT( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl ;
int InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl( struct TabPage *pnodeTabPage )
{
	int	nret = 0 ;

	HFONT hFont = ::CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 

	nret = CreateSymbolTreeCtl( pnodeTabPage , hFont ) ;
	if( nret )
	{
		::DeleteObject( hFont );
		return nret;
	}

	nret = CreateQueryResultEditCtl( pnodeTabPage , hFont ) ;
	if( nret )
	{
		::DeleteObject( hFont );
		return nret;
	}

	nret = CreateQueryResultTableCtl( pnodeTabPage , hFont ) ;
	if( nret )
	{
		::DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SQL ;
int InitTabPageControlsAfterLoadFile_SQL( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_SQL , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	// 解析数据库连接配置
	if( pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	/* 刷新右边符号树 */
	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_REDIS ;
int InitTabPageControlsAfterLoadFile_REDIS( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	// 解析REDIS连接配置
	if( pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	// 连接REDIS服务端
	nret = ConnectToRedis( pnodeTabPage ) ;
	if( nret < 0 )
		return nret;

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_PYTHON ;
int InitTabPageControlsAfterLoadFile_PYTHON( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_PYTHON , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_P_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_P_COMMENTBLOCK , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_P_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_P_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LUA ;
int InitTabPageControlsAfterLoadFile_LUA( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_LUA , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LUA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LUA_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LUA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LUA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_PERL ;
int InitTabPageControlsAfterLoadFile_PERL( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_PERL , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_PL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_PL_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_PL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_PL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SH ;
int InitTabPageControlsAfterLoadFile_SH( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_BASH , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_SH_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_SH_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_SH_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_SH_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_RUST ;
int InitTabPageControlsAfterLoadFile_RUST( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_RUST , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTLINEDOC , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTBLOCK , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTBLOCKDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_RUBY ;
int InitTabPageControlsAfterLoadFile_RUBY( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_RUBY , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_WORD_DEMOTED , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_WORD_DEMOTED , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_WORD_DEMOTED , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_WORD_DEMOTED , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_RB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_RB_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_RB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_RB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LISP ;
int InitTabPageControlsAfterLoadFile_LISP( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_LISP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_LISP_MULTI_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_LISP_MULTI_COMMENT , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_LISP_MULTI_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_LISP_MULTI_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_ASM ;
int InitTabPageControlsAfterLoadFile_ASM( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_ASM , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_CPUINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_CPUINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_CPUINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_CPUINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_MATHINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_MATHINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_MATHINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_MATHINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_REGISTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_REGISTER , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_REGISTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_REGISTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_DIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_DIRECTIVE , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_DIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_DIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_DIRECTIVEOPERAND , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_EXTINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_EXTINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_EXTINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_EXTINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_ASM_COMMENTDIRECTIVE , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_COBOL ;
int InitTabPageControlsAfterLoadFile_COBOL( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_COBOL , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 5 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 8 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 16 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 16 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 16 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 16 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 6 , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 7 , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 4 , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 10 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 10 , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 10 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 10 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 9 , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 2 , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 3 , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_HTML ;
int InitTabPageControlsAfterLoadFile_HTML( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_HTML , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAG , g_pstWindowTheme->stStyleTheme.tags.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTEUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ENTITY , g_pstWindowTheme->stStyleTheme.entities.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGEND , g_pstWindowTheme->stStyleTheme.tagends.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_CDATA , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_QUESTION , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_SCRIPT , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ASP , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ASPAT , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );
	}

	/* JavaScript */
	if( pnodeTabPage->pstDocTypeConfig->keywords3 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords3) );

		for( int style = SCE_HJ_START ; style <= SCE_HJ_REGEX ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJ_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJ_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJ_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJ_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HJA_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HJA_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HJA_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HJA_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );
	}

	/* VBScript */
	if( pnodeTabPage->pstDocTypeConfig->keywords4 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 2 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords4) );

		for( int style = SCE_HB_START ; style <= SCE_HB_STRINGEOL ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HB_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HB_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HB_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HB_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HB_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HB_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HB_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HB_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		for( int style = SCE_HBA_START ; style <= SCE_HBA_STRINGEOL ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HBA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HBA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HBA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HBA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HBA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HBA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HBA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HBA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HBA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HBA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HBA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HBA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HBA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HBA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HBA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HBA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HBA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HBA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HBA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HBA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );
	}

	/* Python */
	if( pnodeTabPage->pstDocTypeConfig->keywords5 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 3 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords5) );

		for( int style = SCE_HP_START ; style <= SCE_HP_IDENTIFIER ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

		for( int style = SCE_HPHP_COMPLEX_VARIABLE ; style <= SCE_HPA_IDENTIFIER ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPA_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	}

	/* PHP */
	if( pnodeTabPage->pstDocTypeConfig->keywords6 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 4 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords6) );

		for( int style = SCE_HPHP_DEFAULT ; style <= SCE_HPHP_OPERATOR ; style++ )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_HSTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_HSTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_HSTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_HSTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_SIMPLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_SIMPLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_SIMPLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_SIMPLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_HPHP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_HPHP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );
	}

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CSS ;
int InitTabPageControlsAfterLoadFile_CSS( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CSS , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 2 , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 3 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 4 , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 6 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 7 , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 8 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 9 , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 13 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 13 , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 13 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 13 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , 14 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , 14 , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , 14 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , 14 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JAVASCRIPT ;
int InitTabPageControlsAfterLoadFile_JAVASCRIPT( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	if( pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_XML ;
int InitTabPageControlsAfterLoadFile_XML( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_XML , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAG , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTEUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_ENTITY , g_pstWindowTheme->stStyleTheme.entities.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGEND , g_pstWindowTheme->stStyleTheme.tagends.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_CDATA , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_H_QUESTION , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JSON ;
int InitTabPageControlsAfterLoadFile_JSON( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_JSON , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_PROPERTYNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_PROPERTYNAME , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_PROPERTYNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_PROPERTYNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_LINECOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_LINECOMMENT , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_LINECOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_LINECOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_BLOCKCOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_BLOCKCOMMENT , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_BLOCKCOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_BLOCKCOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_JSON_LDKEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_JSON_LDKEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_JSON_LDKEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_JSON_LDKEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_ERROR , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	pnodeTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_YAML ;
int InitTabPageControlsAfterLoadFile_YAML( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_YAML , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_IDENTIFIER , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_TEXT , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_TEXT , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_TEXT , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_TEXT , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_ERROR , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_YAML_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_YAML_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_YAML_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_YAML_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_MAKEFILE ;
int InitTabPageControlsAfterLoadFile_MAKEFILE( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_MAKEFILE , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MAKE_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MAKE_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MAKE_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_IDENTIFIER , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MAKE_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MAKE_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MAKE_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MAKE_TARGET , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_TARGET , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MAKE_TARGET , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_TARGET , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CMAKE ;
int InitTabPageControlsAfterLoadFile_CMAKE( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CMAKE , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
	}

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGDQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGDQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGDQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGDQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGLQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGLQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGLQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGLQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGRQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGRQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGRQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGRQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_COMMANDS , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_COMMANDS , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_COMMANDS , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_COMMANDS , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_PARAMETERS , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_PARAMETERS , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_PARAMETERS , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_PARAMETERS , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_VARIABLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_VARIABLE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_VARIABLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_VARIABLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_IFDEFINEDEF , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_MACRODEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_MACRODEF , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_MACRODEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_MACRODEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGVAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGVAR , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGVAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGVAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_MARKDOWN ;
int InitTabPageControlsAfterLoadFile_MARKDOWN( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_MARKDOWN , 0 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRONG1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRONG1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRONG1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRONG1 , TRUE );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRONG2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRONG2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRONG2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRONG2 , TRUE );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_EM1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_EM1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_EM1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_EM1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETITALIC , SCE_MARKDOWN_EM1 , TRUE );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_EM2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETITALIC , SCE_MARKDOWN_EM2 , TRUE );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER3 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER4 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER5 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER6 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_PRECHAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_PRECHAR , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_PRECHAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_PRECHAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_ULIST_ITEM , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_OLIST_ITEM , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_BLOCKQUOTE , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRIKEOUT , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_STRIKEOUT , 1 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HRULE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HRULE , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HRULE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HRULE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_HRULE , 999 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_LINK , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_LINK , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_LINK , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_LINK , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_LINK , 99 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODE2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODE2 , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODE2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODE2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODEBK , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.font) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODEBK , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODEBK , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODEBK , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

	// 折叠
	InitScintillaControlFold( pnodeTabPage );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LOG ;
int InitTabPageControlsAfterLoadFile_LOG( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( pnodeTabPage->pstDocTypeConfig->keywords )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( pnodeTabPage->pstDocTypeConfig->keywords2 )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(pnodeTabPage->pstDocTypeConfig->keywords2) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFONT , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.font) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD2 , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );
	}

	return 0;
}

funcParseFileConfigHeader ParseSqlFileConfigHeader ;
int ParseSqlFileConfigHeader( struct TabPage *pnodeTabPage )
{
	struct DatabaseConnectionConfig	stDatabaseConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stDatabaseConnectionConfig , 0x00 , sizeof(struct DatabaseConnectionConfig) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( pnodeTabPage , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_DATABASE_CONNECTION_CONFIG , sizeof(BEGIN_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_DATABASE_CONNECTION_CONFIG , sizeof(END_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stDatabaseConnectionConfig.dbtype[0] )
				{
					BOOL	bChangeFlag = FALSE ;

					if(
						( pnodeTabPage->stDatabaseConnectionConfig.dbtype[0] && strcmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , stDatabaseConnectionConfig.dbtype ) )
						||
						( pnodeTabPage->stDatabaseConnectionConfig.dbhost[0] && strcmp( pnodeTabPage->stDatabaseConnectionConfig.dbhost , stDatabaseConnectionConfig.dbhost ) )
						||
						( pnodeTabPage->stDatabaseConnectionConfig.dbport && pnodeTabPage->stDatabaseConnectionConfig.dbport != stDatabaseConnectionConfig.dbport )
						||
						( pnodeTabPage->stDatabaseConnectionConfig.dbuser[0] && strcmp( pnodeTabPage->stDatabaseConnectionConfig.dbuser , stDatabaseConnectionConfig.dbuser ) )
						||
						( pnodeTabPage->stDatabaseConnectionConfig.dbpass[0] && strcmp( pnodeTabPage->stDatabaseConnectionConfig.dbpass , stDatabaseConnectionConfig.dbpass ) )
						||
						( pnodeTabPage->stDatabaseConnectionConfig.dbname[0] && strcmp( pnodeTabPage->stDatabaseConnectionConfig.dbname , stDatabaseConnectionConfig.dbname ) )
					)
					{
						bChangeFlag = TRUE ;
					}

					if( stDatabaseConnectionConfig.dbpass[0] )
						stDatabaseConnectionConfig.bConfigDbPass = TRUE ;
					else
						stDatabaseConnectionConfig.bConfigDbPass = FALSE ;

					memcpy( & (pnodeTabPage->stDatabaseConnectionConfig) , & stDatabaseConnectionConfig , sizeof(struct DatabaseConnectionConfig) );

					if( bChangeFlag == TRUE )
					{
						DisconnectFromDatabase( pnodeTabPage );

						ConnectToDatabase( pnodeTabPage );
					}
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%*[ ]%[^\n]" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "DBTYPE" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbtype , acConfigValue , sizeof(stDatabaseConnectionConfig.dbtype)-1 );
			else if( strcmp( acConfigKey , "DBHOST" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbhost , acConfigValue , sizeof(stDatabaseConnectionConfig.dbhost)-1 );
			else if( strcmp( acConfigKey , "DBPORT" ) == 0 )
				stDatabaseConnectionConfig.dbport = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "DBUSER" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbuser , acConfigValue , sizeof(stDatabaseConnectionConfig.dbuser)-1 );
			else if( strcmp( acConfigKey , "DBPASS" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbpass , acConfigValue , sizeof(stDatabaseConnectionConfig.dbpass)-1 );
			else if( strcmp( acConfigKey , "DBNAME" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbname , acConfigValue , sizeof(stDatabaseConnectionConfig.dbname)-1 );
		}
	}

	return 0;
}

funcParseFileConfigHeader ParseRedisFileConfigHeader ;
int ParseRedisFileConfigHeader( struct TabPage *pnodeTabPage )
{
	struct RedisConnectionConfig	stRedisConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stRedisConnectionConfig , 0x00 , sizeof(struct RedisConnectionConfig) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( pnodeTabPage , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_REDIS_CONNECTION_CONFIG , sizeof(BEGIN_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_REDIS_CONNECTION_CONFIG , sizeof(END_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stRedisConnectionConfig.host[0] )
				{
					BOOL	bChangeFlag = FALSE ;

					if(
						( pnodeTabPage->stRedisConnectionConfig.host[0] && strcmp(pnodeTabPage->stRedisConnectionConfig.host,stRedisConnectionConfig.host) )
						||
						( pnodeTabPage->stRedisConnectionConfig.port && pnodeTabPage->stRedisConnectionConfig.port != stRedisConnectionConfig.port )
						||
						( pnodeTabPage->stRedisConnectionConfig.pass[0] && strcmp(pnodeTabPage->stRedisConnectionConfig.pass,stRedisConnectionConfig.pass) )
						||
						( pnodeTabPage->stRedisConnectionConfig.dbsl[0] && strcmp(pnodeTabPage->stRedisConnectionConfig.dbsl,stRedisConnectionConfig.dbsl) )
					)
					{
						bChangeFlag = TRUE ;
					}

					memcpy( & (pnodeTabPage->stRedisConnectionConfig) , & stRedisConnectionConfig , sizeof(struct RedisConnectionConfig) );

					if( bChangeFlag == TRUE )
					{
						DisconnectFromRedis( pnodeTabPage );

						ConnectToRedis( pnodeTabPage );
					}
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%s" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "HOST" ) == 0 )
				strncpy( stRedisConnectionConfig.host , acConfigValue , sizeof(stRedisConnectionConfig.host)-1 );
			else if( strcmp( acConfigKey , "PORT" ) == 0 )
				stRedisConnectionConfig.port = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "PASS" ) == 0 )
				strncpy( stRedisConnectionConfig.pass , acConfigValue , sizeof(stRedisConnectionConfig.pass)-1 );
			else if( strcmp( acConfigKey , "DBSL" ) == 0 )
				strncpy( stRedisConnectionConfig.dbsl , acConfigValue , sizeof(stRedisConnectionConfig.dbsl)-1 );
		}
	}

	return 0;
}

int OnCharAdded_AutoAddClosedCharacter( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	/* 自动补全关闭符号 */
	if( lpnotify->ch == '(' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)")" ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
	}
	else if( lpnotify->ch == '[' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"]" ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
	}
	else if( lpnotify->ch == '{' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"}" ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
	}
	/*
	else if( lpnotify->ch == '<' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)">" ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
	}
	*/
	else if( lpnotify->ch == '\'' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		char chCurrentCharacter = (char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nCurrentPos , 0 ) ;
		char chPrevPrevCharacter = 0 ;
		if( nCurrentPos >= 2 )
		{
			chPrevPrevCharacter = (char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nCurrentPos-2 , 0 ) ;
		}
		if( chPrevPrevCharacter == '\'' || chCurrentCharacter == '\'' )
		{
			;
		}
		else
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"'" ) ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
		}
	}
	else if( lpnotify->ch == '"' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		char chCurrentCharacter = (char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nCurrentPos , 0 ) ;
		char chPrevPrevCharacter = 0 ;
		if( nCurrentPos >= 2 )
		{
			chPrevPrevCharacter = (char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nCurrentPos-2 , 0 ) ;
		}
		if( chPrevPrevCharacter == '"' || chCurrentCharacter == '"' )
		{
			;
		}
		else
		{
			int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"\"" ) ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
		}
	}

	return 0;
}

int OnCharAdded_AutoAddClosedCharacter_AngleBracket( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	/* 自动补全关闭符号 */
	if( lpnotify->ch == '<' && g_stEditUltraMainConfig.bEnableAutoAddCloseChar )
	{
		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)">" ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
	}

	return 0;
}

int OnCharAdded_AutoIdentation( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	/* 自动缩进 */
	if( lpnotify->ch == '\n' && g_stEditUltraMainConfig.bEnableAutoIdentation )
	{
		int	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		int	nCurrnetLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;
		int	nPrevLinePos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_POSITIONFROMLINE , nCurrnetLine-1 , 0 ) ;
		char	acPrevLineIndentation[ 64 ] ;
		int	nPrevLineIndentationLen = 0 ;
		int	nPos = nPrevLinePos ;
		bool	bCollectIndentation = true ;
		int	nLineTailChar = 0 ;

		memset( acPrevLineIndentation , 0x00 , sizeof(acPrevLineIndentation) );
		for( int ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 ) ; ch != '\n' ; nPos++ , ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 ) )
		{
			if( bCollectIndentation == true )
			{
				if( strchr( " \t" , ch ) && nPrevLineIndentationLen <= sizeof(acPrevLineIndentation)-1 )
				{
					acPrevLineIndentation[nPrevLineIndentationLen] = ch ;
					nPrevLineIndentationLen++;
					continue;
				}
				else
				{
					bCollectIndentation = false ;
				}
			}

			if( ! strchr( " \t\r\n" , ch ) )
				nLineTailChar = ch ;
		}

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acPrevLineIndentation ) ;
		if( nLineTailChar && strchr( "([{<" , nLineTailChar ) )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"\t" ) ;

			nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)(pnodeTabPage->acEndOfLine) ) ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acPrevLineIndentation ) ;

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS , (sptr_t)nCurrentPos , 0 );
		}
	}

	return 0;
}

int OnCharAdded_CallTipsOnInputParenthesis( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( lpnotify->ch == '(' && g_stEditUltraMainConfig.bEnableCallTipShow )
	{
		/* 函数原型提示 */
		if( pnodeTabPage->pstDocTypeConfig &&  ! RB_EMPTY_ROOT(&(pnodeTabPage->pstDocTypeConfig->stCallTipShowTree.treeCallTipShow)) )
		{
			int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDSTARTPOSITION , nCurrentPos-1 , true ) ;
			int nEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDENDPOSITION , nCurrentPos-1 , true ) ;
			if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
				nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

			Sci_TextRange	tr ;
			memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
			tr.chrg.cpMin = nStartPos;
			tr.chrg.cpMax = nEndPos; 
			tr.lpstrText = acWordBuffer; 
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );

			struct CallTipShowNode ctsQuery ;
			memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
			ctsQuery.acCallTipFuncName = acWordBuffer ;
			struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (pnodeTabPage->pstDocTypeConfig->stCallTipShowTree) , & ctsQuery ) ;
			if( p_ctsLocation )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPSHOW , nCurrentPos , sptr_t(p_ctsLocation->acCallTipFuncDesc) );
			}
			else
			{
				if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}
	else if( lpnotify->ch == ',' && g_stEditUltraMainConfig.bEnableCallTipShow )
	{
		/* 函数原型提示 */
		if( pnodeTabPage->pstDocTypeConfig && ! RB_EMPTY_ROOT(&(pnodeTabPage->pstDocTypeConfig->stCallTipShowTree.treeCallTipShow)) )
		{
			int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) - 1 ;
			int nPos = nCurrentPos ;
			int ch ;
			for( ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 )
				; nPos >= 0 && ch != '('
				; nPos-- , ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 ) )
				;
			if( ch == '(' && nPos >= 0 )
			{
				int nStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDSTARTPOSITION , nPos-1 , true ) ;
				int nEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDENDPOSITION , nPos-1 , true ) ;
				if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
					nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

				Sci_TextRange	tr ;
				memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
				tr.chrg.cpMin = nStartPos;
				tr.chrg.cpMax = nEndPos; 
				tr.lpstrText = acWordBuffer; 
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );

				struct CallTipShowNode ctsQuery ;
				memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
				ctsQuery.acCallTipFuncName = acWordBuffer ;
				struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (pnodeTabPage->pstDocTypeConfig->stCallTipShowTree) , & ctsQuery ) ;
				if( p_ctsLocation )
				{
					pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPSHOW , nCurrentPos , sptr_t(p_ctsLocation->acCallTipFuncDesc) );
				}
				else
				{
					if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
						pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
				}
			}
			else
			{
				if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}

	return 0;
}

int OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( struct TabPage *pnodeTabPage , SCNotification *lpnotify , char chFromOneCharacter , BOOL bUpperCase )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( lpnotify->ch == ' ' && g_stEditUltraMainConfig.bEnableCallTipShow )
	{
		/* 函数原型提示 */
		if( pnodeTabPage->pstDocTypeConfig &&  ! RB_EMPTY_ROOT(&(pnodeTabPage->pstDocTypeConfig->stCallTipShowTree.treeCallTipShow)) )
		{
			int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = nCurrentPos ;
			int nEndPos  ;
			int ch ;

			while(1)
			{
				nStartPos--;
				if( nStartPos < 0 )
				{
					nStartPos = 0 ;
					break;
				}

				ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nStartPos , 0 ) ;
				if( ch == chFromOneCharacter )
				{
					nStartPos++;
					break;
				}
			}

			while(1)
			{
				ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nStartPos , 0 ) ;
				if( ! strchr( " \t\r\n\f" , ch ) )
				{
					break;
				}

				nStartPos++;
				if( nStartPos >= nCurrentPos )
				{
					break;
				}
			}

			nEndPos = nStartPos + 1 ;
			while(1)
			{
				ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nEndPos , 0 ) ;
				if( strchr( " \t\r\n\f" , ch ) )
				{
					break;
				}

				nEndPos++;
				if( nEndPos >= nCurrentPos )
				{
					break;
				}
			}

			Sci_TextRange	tr ;
			memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
			tr.chrg.cpMin = nStartPos;
			tr.chrg.cpMax = nEndPos; 
			tr.lpstrText = acWordBuffer; 
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );

			if( bUpperCase == TRUE )
				ToUpperString( acWordBuffer );

			struct CallTipShowNode ctsQuery ;
			memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
			ctsQuery.acCallTipFuncName = acWordBuffer ;
			struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (pnodeTabPage->pstDocTypeConfig->stCallTipShowTree) , & ctsQuery ) ;
			if( p_ctsLocation )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPSHOW , nCurrentPos , sptr_t(p_ctsLocation->acCallTipFuncDesc) );
			}
			else
			{
				if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}
	else if( lpnotify->ch == '\n' && g_stEditUltraMainConfig.bEnableCallTipShow )
	{
		if( pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
	}

	return 0;
}

int OnCharAdded_AutoCompletedShow( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( -1 <= lpnotify->ch && lpnotify->ch <= 255 && isprint(lpnotify->ch) && lpnotify->ch != ' ' && lpnotify->ch != '\t' && g_stEditUltraMainConfig.bEnableAutoCompletedShow )
	{
		/* 自动完成提示 */
		if( pnodeTabPage->pstDocTypeConfig && ! RB_EMPTY_ROOT(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) )
		{
			int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDSTARTPOSITION , nCurrentPos-1 , true ) ;
			int nEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDENDPOSITION , nCurrentPos-1 , true ) ;
			if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
				nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;
			if( nEndPos-nStartPos >= g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters )
			{
				Sci_TextRange	tr ;
				memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
				tr.chrg.cpMin = nStartPos;
				tr.chrg.cpMax = nEndPos; 
				tr.lpstrText = acWordBuffer; 
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
				if( acWordBuffer[0] )
				{
					struct AutoCompletedShowNode acsQuery ;
					memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
					acsQuery.acAutoCompletedString = acWordBuffer ;
					struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree) , & acsQuery ) ;
					if( p_acsLocation )
					{
						struct AutoCompletedShowNode *p_acsStart = NULL ;
						struct AutoCompletedShowNode *p_acsEnd = NULL ;

						struct rb_node *p_nodeFirst = rb_first(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) ;
						struct rb_node *p_nodeLast = rb_last(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) ;

						struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
						struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
						if( p_nodePrev && p_nodeNext )
						{
							struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							p_acsStart = p_acsLocation ;
							while(1)
							{
								nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
								if( nret )
									break;
								else
									p_acsStart = p_acsPrev ;

								if( p_nodePrev == p_nodeFirst )
									break;
								p_nodePrev = rb_prev(p_nodePrev) ;
								p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							}

							struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							p_acsEnd = p_acsLocation ;
							while(1)
							{
								nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
								if( nret )
									break;
								else
									p_acsEnd = p_acsNext ;

								if( p_nodeNext == p_nodeLast )
									break;
								p_nodeNext = rb_next(p_nodeNext) ;
								p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							}

							pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos = p_acsStart->begin ;
							pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos = p_acsEnd->end ;

							sprintf( pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer , "%.*s" , pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos - pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos - 1 , pnodeTabPage->pstDocTypeConfig->acAutoCompletedBuffer + pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos );
							pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCSHOW , nCurrentPos-nStartPos , sptr_t(pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer) );
						}
					}
				}
			}
		}
	}

	return 0;
}

int OnCharAdded_AutoCompletedShow_HTML( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( -1 <= lpnotify->ch && lpnotify->ch <= 255 && isprint(lpnotify->ch) && lpnotify->ch != '\t' && g_stEditUltraMainConfig.bEnableAutoCompletedShow )
	{
		/* 自动完成提示 */
		if( pnodeTabPage->pstDocTypeConfig && ! RB_EMPTY_ROOT(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) )
		{
			if( lpnotify->ch == '<' )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCSHOW , 0 , sptr_t(pnodeTabPage->pstDocTypeConfig->acAutoCompletedBuffer) );
			}
			else if( lpnotify->ch == ' ' )
			{
				int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nPos ;
				int ch ;
				int chPrev ;

				ch = 0 ;
				for( nPos = nCurrentPos - 1 ; nPos >= 0 ; nPos-- )
				{
					chPrev = ch ;
					ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 ) ;
					if( ch == '<' || ch == '>' )
						break;
				}
				if( nPos >= 0 && ch == '<' && chPrev != '?' && chPrev != '%' )
				{
					pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCSHOW , 0 , sptr_t(pnodeTabPage->pstDocTypeConfig->acAutoCompletedBuffer2) );
				}
			}
			else if( isalpha(lpnotify->ch) )
			{
				int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nPos ;
				int ch ;
				int chPrev ;

				int nStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDSTARTPOSITION , nCurrentPos-1 , true ) ;
				int nEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_WORDENDPOSITION , nCurrentPos-1 , true ) ;
				if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
					nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

				if( nStartPos > 0 && nEndPos-nStartPos >= g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters )
				{
					BOOL	bIsAttribute = FALSE ;

					ch = 0 ;
					for( nPos = nCurrentPos ; nPos >= 0 ; nPos-- )
					{
						chPrev = ch ;
						ch = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nPos , 0 ) ;
						if( ch == ' ' )
							bIsAttribute = TRUE ;
						else if( ch == '<' || ch == '>' )
							break;
					}
					if( nPos >= 0 )
					{
						if( ch == '<' && chPrev != '?' && chPrev != '%' )
						{
							if( bIsAttribute == FALSE )
							{
								Sci_TextRange	tr ;
								memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
								tr.chrg.cpMin = nStartPos;
								tr.chrg.cpMax = nEndPos; 
								tr.lpstrText = acWordBuffer; 
								pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
								if( acWordBuffer[0] )
								{
									struct AutoCompletedShowNode acsQuery ;
									memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
									acsQuery.acAutoCompletedString = acWordBuffer ;
									struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree) , & acsQuery ) ;
									if( p_acsLocation )
									{
										struct AutoCompletedShowNode *p_acsStart = NULL ;
										struct AutoCompletedShowNode *p_acsEnd = NULL ;

										struct rb_node *p_nodeFirst = rb_first(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) ;
										struct rb_node *p_nodeLast = rb_last(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree.treeAutoCompletedShow)) ;

										struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
										struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
										if( p_nodePrev && p_nodeNext )
										{
											struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsStart = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
												if( nret )
													break;
												else
													p_acsStart = p_acsPrev ;

												if( p_nodePrev == p_nodeFirst )
													break;
												p_nodePrev = rb_prev(p_nodePrev) ;
												p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsEnd = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
												if( nret )
													break;
												else
													p_acsEnd = p_acsNext ;

												if( p_nodeNext == p_nodeLast )
													break;
												p_nodeNext = rb_next(p_nodeNext) ;
												p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos = p_acsStart->begin ;
											pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos = p_acsEnd->end ;

											sprintf( pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer , "%.*s" , pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos - pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos - 1 , pnodeTabPage->pstDocTypeConfig->acAutoCompletedBuffer + pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos );
											pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCSHOW , nCurrentPos-nStartPos , sptr_t(pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer) );
										}
									}
								}
							}
							else
							{
								Sci_TextRange	tr ;
								memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
								tr.chrg.cpMin = nStartPos;
								tr.chrg.cpMax = nEndPos; 
								tr.lpstrText = acWordBuffer; 
								pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXTRANGE , 0 , sptr_t(&tr) );
								if( acWordBuffer[0] )
								{
									struct AutoCompletedShowNode acsQuery ;
									memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
									acsQuery.acAutoCompletedString = acWordBuffer ;
									struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree2) , & acsQuery ) ;
									if( p_acsLocation )
									{
										struct AutoCompletedShowNode *p_acsStart = NULL ;
										struct AutoCompletedShowNode *p_acsEnd = NULL ;

										struct rb_node *p_nodeFirst = rb_first(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree2.treeAutoCompletedShow)) ;
										struct rb_node *p_nodeLast = rb_last(&(pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree2.treeAutoCompletedShow)) ;

										struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
										struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
										if( p_nodePrev && p_nodeNext )
										{
											struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsStart = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
												if( nret )
													break;
												else
													p_acsStart = p_acsPrev ;

												if( p_nodePrev == p_nodeFirst )
													break;
												p_nodePrev = rb_prev(p_nodePrev) ;
												p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsEnd = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
												if( nret )
													break;
												else
													p_acsEnd = p_acsNext ;

												if( p_nodeNext == p_nodeLast )
													break;
												p_nodeNext = rb_next(p_nodeNext) ;
												p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos2 = p_acsStart->begin ;
											pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos2 = p_acsEnd->end ;

											sprintf( pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer2 , "%.*s" , pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowEndPos2 - pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos2 - 1 , pnodeTabPage->pstDocTypeConfig->acAutoCompletedBuffer2 + pnodeTabPage->pstDocTypeConfig->nAutoCompletedShowStartPos2 );
											pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_AUTOCSHOW , nCurrentPos-nStartPos , sptr_t(pnodeTabPage->pstDocTypeConfig->acAutoCompletedShowBuffer2) );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0;
}

funcOnCharAdded OnCharAdded_CPP_like ;
int OnCharAdded_CPP_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputParenthesis( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_SQL_like ;
int OnCharAdded_SQL_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( pnodeTabPage , lpnotify , ';' , TRUE ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_REDIS_like ;
int OnCharAdded_REDIS_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( pnodeTabPage , lpnotify , '\n' , TRUE ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_HTML_like ;
int OnCharAdded_HTML_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter_AngleBracket( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow_HTML( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_XML_like ;
int OnCharAdded_XML_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter_AngleBracket( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_CSS_like ;
int OnCharAdded_CSS_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_JSON_like ;
int OnCharAdded_JSON_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_YAML_like ;
int OnCharAdded_YAML_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_MAKEFILE_like ;
int OnCharAdded_MAKEFILE_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_CMAKE_like ;
int OnCharAdded_CMAKE_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_MARKDOWN_like ;
int OnCharAdded_MARKDOWN_like( struct TabPage *pnodeTabPage , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( pnodeTabPage , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

int OnKeyDown_BraceHighLight( struct TabPage *pnodeTabPage )
{
	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	char chCurrentCharacter = (char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCHARAT , nCurrentPos , 0 ) ;

	if( chCurrentCharacter && strchr( "()[]{}<>" , chCurrentCharacter ) )
	{
		int nMatchPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_BRACEMATCH , nCurrentPos , 0 ) ;
		if( nMatchPos != -1 )
		{
			int nStyle = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSTYLEAT , nCurrentPos , 0 );
			int nForeColor = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLEGETFORE , nStyle , 0 );
			int nBackColor = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLEGETBACK , nStyle , 0 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , STYLE_BRACELIGHT , nBackColor );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , STYLE_BRACELIGHT , nForeColor );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , STYLE_BRACELIGHT , true );

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_BRACEHIGHLIGHT , nCurrentPos , nMatchPos ) ;
		}
		else
		{
			int nStyle = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSTYLEAT , nCurrentPos , 0 );
			int nForeColor = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLEGETFORE , nStyle , 0 );
			int nBackColor = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLEGETBACK , nStyle , 0 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , STYLE_BRACEBAD , nForeColor );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBACK , STYLE_BRACEBAD , nBackColor );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETITALIC , STYLE_BRACEBAD , true );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETUNDERLINE , STYLE_BRACEBAD , true );

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_BRACEBADLIGHT , nCurrentPos , 0 ) ;
		}
	}
	else
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_BRACEBADLIGHT , INVALID_POSITION , INVALID_POSITION ) ;
	}
	
	return 0;
}

funcOnKeyDown OnKeyDown_GotoSymbolDefineLine ;
int OnKeyDown_GotoSymbolDefineLine( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F11 )
	{
		nret = GetCurrentWordAndJumpGotoLine( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

static void SelectSqlStatement(struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nMaxPos ;
	int	nStartPos ;
	int	nEndPos ;

	nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETTEXTLENGTH, 0, 0 ) ;

	for( nStartPos = nCurrentPos ; nStartPos >= 0 ; nStartPos-- )
	{
		if( ! strchr( "\r\n" , (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) ) )
			break;
	}
	if( (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) == ';' )
		nStartPos--;
	nEndPos = nStartPos ;

	for( ; nStartPos >= 0 ; nStartPos-- )
	{
		if( (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) == ';' )
			break;
	}
	if( nStartPos < 0 )
		nStartPos = 0 ;
	else
		nStartPos++;

	for( ; nEndPos < nMaxPos ; nEndPos++ )
	{
		if( (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCHARAT, nEndPos, 0 ) == ';' )
			break;
	}
	if( nEndPos >= nMaxPos )
		nEndPos = nMaxPos - 1 ;
	/*
	else
	nEndPos--;
	*/

	g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETSEL, nStartPos, nEndPos ) ;

	return;
}

funcOnKeyDown OnKeyDown_SQL ;
int OnKeyDown_SQL( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F5 )
	{
		if( lParam == VK_CONTROL )
		{
			SelectSqlStatement( pnodeTabPage );
		}

		nret = ExecuteSqlQuery( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcOnKeyDown OnKeyDown_REDIS ;
int OnKeyDown_REDIS( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F5 )
	{
		if( lParam == VK_CONTROL )
		{
			SelectSqlStatement( pnodeTabPage );
		}

		nret = ExecuteRedisQuery_REDIS( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcOnKeyUp OnKeyUp_GENERAL ;
int OnKeyUp_GENERAL( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam )
{
	OnKeyDown_BraceHighLight( pnodeTabPage );

	return 0;
}

funcOnReloadSymbolList OnReloadSymbolList_WithReqularExp ;
int OnReloadSymbolList_WithReqularExp( struct TabPage *pnodeTabPage )
{
	int nret = ReloadSymbolList_WithReqularExp( pnodeTabPage );
	return nret;
}

funcOnDbClickSymbolList OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine ;
int OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine( struct TabPage *pnodeTabPage )
{
	return GetSymbolListItemAndJumpGotoLine( pnodeTabPage );
}

funcOnReloadSymbolTree OnReloadSymbolTree_SQL ;
int OnReloadSymbolTree_SQL( struct TabPage *pnodeTabPage )
{
	int nret = ReloadSymbolTree_SQL( pnodeTabPage );
	return nret;
}

funcOnReloadSymbolTree OnReloadSymbolTree_XML ;
int OnReloadSymbolTree_XML( struct TabPage *pnodeTabPage )
{
	HANDLE mutex = ::CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
	if( ! mutex )
		return -1;

	int nret = ReloadSymbolTree_XML( pnodeTabPage );

	CloseHandle(mutex);

	return nret;
}

funcOnReloadSymbolTree OnReloadSymbolTree_JSON ;
int OnReloadSymbolTree_JSON( struct TabPage *pnodeTabPage )
{
	HANDLE mutex = ::CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
	if( ! mutex )
		return -1;

	int nret = ReloadSymbolTree_JSON( pnodeTabPage );

	CloseHandle(mutex);

	return nret;
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_SQL ;
int OnDbClickSymbolTree_SQL( struct TabPage *pnodeTabPage )
{
	return GetSymbolTreeItemAndAddTextToEditor( pnodeTabPage );
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_JSON ;
int OnDbClickSymbolTree_JSON( struct TabPage *pnodeTabPage )
{
	return GetSymbolTreeItemAndGotoEditorPos( pnodeTabPage );
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_REDIS ;
int OnDbClickSymbolTree_REDIS( struct TabPage *pnodeTabPage )
{
	return GetSymbolTreeItemAndAddTextToEditor( pnodeTabPage );
}

funcCleanTabPageControls CleanTabPageControls_AllControls ;
int CleanTabPageControls_AllControls( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->pcreSymbolRe )
	{
		pcre_free( pnodeTabPage->pcreSymbolRe ); pnodeTabPage->pcreSymbolRe = NULL ;
	}

	if( pnodeTabPage->hwndSymbolList )
	{
		DestroyWindow( pnodeTabPage->hwndSymbolList ); pnodeTabPage->hwndSymbolList = NULL ;
	}

	if( pnodeTabPage->hwndSymbolTree )
	{
		DestroyWindow( pnodeTabPage->hwndSymbolTree ); pnodeTabPage->hwndSymbolTree = NULL ;
	}

	if( pnodeTabPage->hwndQueryResultEdit )
	{
		DestroyWindow( pnodeTabPage->hwndQueryResultEdit ); pnodeTabPage->hwndQueryResultEdit = NULL ;
	}

	if( pnodeTabPage->hwndQueryResultTable )
	{
		DestroyWindow( pnodeTabPage->hwndQueryResultTable ); pnodeTabPage->hwndQueryResultTable = NULL ;
	}

	return 0;
}

/*
#define DOCTYPE_KEYWORDS_CPP		(char *)"#define #elif #else #endif #error #if #ifdef #ifndef #include #include_next #line #pragma #undef __asm __based __cdecl __declspec __except __far __fastcall __finally __fortran __huge __inline __int16 __int32 __int64 __int8 __interrupt __leave __loadds __near __pascal __saveregs __segment __segname __self __stdcall __try __uuidof auto bool break case char const continue default defined do double else enum extern float for goto if int long register return short signed sizeof static struct switch typedef union unsigned void volatile while __multiple_inheritance __single_inheritance __virtual_inheritance catch class const_cast delete dynamic_cast explicit export false friend inline mutable namespace new operator private protected public reinterpret_cast static_cast template this throw true try typeid typename using virtual wchar_t dllexport dllimport naked thread uuid"
#define DOCTYPE_KEYWORDS2_CPP		(char *)"a64l abort abs asprintf __asprintf atexit atof atoi atol atoll at_quick_exit available basename bcmp bcopy bsearch __builtin_alloca __builtin_index __builtin_memchr __builtin_rindex __builtin_strchr __builtin_strpbrk __builtin_strrchr __builtin_strstr bzero __bzero calloc canonicalize_file_name cfree clearenv clearerr clearerr_unlocked Copyright ctermid __ctype_get_mb_cur_max cuserid DEST div dprintf drand48 drand48_r ecvt ecvt_r EOF erand48 erand48_r exit _Exit __extension__ fclose fcloseall fcvt fcvt_r fdopen feof feof_unlocked ferror ferror_unlocked fflush fflush_unlocked ffs ffsl ffsll fgetc fgetc_unlocked fgetpos fgetpos64 fgets fgets_unlocked fileno fileno_unlocked flockfile fmemopen fopen fopen64 fopencookie __format__ fprintf fputc fputc_unlocked fputs fputs_unlocked fread fread_unlocked free freopen freopen64 fscanf fseek fseeko fseeko64 fsetpos fsetpos64 ftell ftello ftello64 ftrylockfile funlockfile fwrite fwrite_unlocked gcvt getc getchar getchar_unlocked getc_unlocked getdelim __getdelim getenv getline getloadavg getpt gets getsubopt getw __GNUC_PREREQ grantpt if index initstate initstate_r int _IO_getc _IO_putc __isoc99_fscanf __isoc99_scanf __isoc99_sscanf __isoc99_vfscanf __isoc99_vscanf __isoc99_vsscanf jrand48 jrand48_r l64a labs lcong48 lcong48_r ldiv llabs lldiv lrand48 lrand48_r malloc mblen mbstowcs mbtowc memccpy memchr memcmp memcpy memfrob memmem memmove mempcpy __mempcpy memrchr memset mkdtemp mkostemp mkostemp64 mkostemps mkostemps64 mkstemp mkstemp64 mkstemps mkstemps64 mktemp mrand48 mrand48_r nrand48 nrand48_r __NTH obstack_printf obstack_vprintf on_exit open_memstream optimizations or pclose perror popen posix_memalign posix_openpt printf ptsname ptsname_r putc putchar putchar_unlocked putc_unlocked putenv puts putw qecvt qecvt_r qfcvt qfcvt_r qgcvt qsort qsort_r quick_exit rand random random_r rand_r rawmemchr read realloc realpath remove rename renameat return rewind rindex rpmatch S S2 scanf __secure_getenv seed48 seed48_r setbuf setbuffer setenv setkey setlinebuf setstate setstate_r setvbuf snprintf sprintf srand srand48 srand48_r srandom srandom_r sscanf standard stpcpy __stpcpy stpncpy __stpncpy strcasecmp strcasecmp_l strcasestr strcat strchr strchrnul strcmp strcoll strcoll_l strcpy strcspn strdup strdupa strerror strerror_l strerror_r strfry strlen strncasecmp strncasecmp_l strncat strncmp strncpy strndup strndupa strnlen strpbrk strrchr strsep strsignal strspn strstr strtod strtod_l strtof strtof_l strtok __strtok_r strtok_r strtol strtold strtold_l strtoll strtol_l strtoll_l strtoq strtoul strtoull strtoul_l strtoull_l strtouq strverscmp strxfrm strxfrm_l system tempnam tmpfile tmpfile64 tmpnam tmpnam_r to __typeof ungetc unlockpt unsetenv __USING_NAMESPACE_STD valloc vasprintf vdprintf vfprintf vfscanf void vprintf vscanf vsnprintf vsprintf vsscanf __WAIT_INT wcstombs wctomb WEXITSTATUS __WEXITSTATUS WIFCONTINUED __WIFCONTINUED WIFEXITED __WIFEXITED WIFSIGNALED __WIFSIGNALED WIFSTOPPED __WIFSTOPPED word written WSTOPSIG __WSTOPSIG WTERMSIG __WTERMSIG __xpg_strerror_r"
*/
// #define DOCTYPE_STYLE_SET_DEFAULT	0x00FF0000,TRUE , 0x000000FF,TRUE , 0x001515A3,FALSE , 0x001515A3,FALSE , 0x00808080,FALSE , 0x00008000,FALSE , 0x00008000,FALSE , 0x00008000,FALSE , 0x00FFC0CB,FALSE

struct DocTypeConfig	g_astDocTypeConfig[] =
	{
		{ DOCTYPE_TXT , NULL , ";*.txt;*.ini;*.conf;" , "Text Files" , NULL , NULL , NULL , NULL , NULL , OnCharAdded_AutoIdentation , NULL , NULL , NULL , NULL , NULL } ,
		{ DOCTYPE_CPP , "doctype_CPP.conf" , ";*.h;*.hh;*.hpp;*.c;*.cc;*.cpp;*.cxx;*.rc;*.rc2;*.dlg;" , "C/C++ Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_CPP , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CS , "doctype_CS.conf" , ";*.cs;" , "C# Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_CS , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JAVA , "doctype_JAVA.conf" , ";*.java;*.jad;*.pde;" , "Java Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_JAVA , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_GO , "doctype_GO.conf" , ";*.go;" , "Go Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_GO , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SWIFT , "doctype_SWIFT.conf" , ";*.swift;" , "Swift Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_SWIFT , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SQL , "doctype_SQL.conf" , ";*.sql;" , "SQL Command Files" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl , InitTabPageControlsAfterLoadFile_SQL , ParseSqlFileConfigHeader , OnKeyDown_SQL , OnKeyUp_GENERAL , OnCharAdded_SQL_like , NULL , NULL , OnReloadSymbolTree_SQL , OnDbClickSymbolTree_SQL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_REDIS , "doctype_REDIS.conf" , ";*.redis;" , "Redis Command Files" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_REDIS , ParseRedisFileConfigHeader , OnKeyDown_REDIS , OnKeyUp_GENERAL , OnCharAdded_REDIS_like , NULL , OnDbClickSymbolTree_REDIS , NULL , OnDbClickSymbolTree_REDIS , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_PYTHON , "doctype_PYTHON.conf" , ";*.py;*.pyw;*.pyx;*.pxd;*.pxi;" , "Python Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_PYTHON , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LUA , "doctype_LUA.conf" , ";*.lua;" , "Lua Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_LUA , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_PERL , "doctype_PERL.conf" , ";*.pl;*.perl;" , "Perl Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_PERL , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SH , "doctype_SH.conf" , ";*.sh;" , "Shell Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_SH , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_RUST , "doctype_RUST.conf" , ";*.rs;" , "Rust Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_RUST , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_RUBY , "doctype_RUBY.conf" , ";*.rb;" , "Ruby Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_RUBY , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LISP , "doctype_LISP.conf" , ";*.lsp;" , "Lisp Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_LISP , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_ASM , "doctype_ASM.conf" , ";*.asm;" , "Asm Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_ASM , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_COBOL , "doctype_COBOL.conf" , ";*.cobol;*.cob;" , "Cobol Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_COBOL , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_HTML , "doctype_HTML.conf" , ";*.html;*.htm;*.shtml;*.xhtml;*.phtml;*.htt;*.htd;*.hta;*.asp;*.php;" , "HTML Files" , NULL , InitTabPageControlsAfterLoadFile_HTML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_HTML_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CSS , "doctype_CSS.conf" , ";*.css;" , "CSS Files" , NULL , InitTabPageControlsAfterLoadFile_CSS , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CSS_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JAVASCRIPT , "doctype_JAVASCRIPT.conf" , ";*.js;*.es;*.ts;*.jse;*.jsm;*.mjs;*.qs;" , "JavaScript Language Files" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_JAVASCRIPT , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_XML , "doctype_XML.conf" , ";*.xml;*.xsl;*.svg;*.xul;*.xsd;*.dtd;*.xslt;*.axl;*.xrc;*.rdf;" , "XML Files" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_XML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_XML_like , NULL , NULL , OnReloadSymbolTree_XML , OnDbClickSymbolTree_JSON , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JSON , "doctype_JSON.conf" , ";*.json;*.eslintrc;*.jshintrc;*.jsonld;*.ipynb;*.babelrc;*.prettierrc;*.stylelintrc;*.jsonc;*.jscop;" , "JSON Files" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_JSON , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_JSON_like , NULL , NULL , OnReloadSymbolTree_JSON , OnDbClickSymbolTree_JSON , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_YAML , "doctype_YAML.conf" , ";*.yaml;*.yml;*.clang-format;*.clang-tidy;*.mir;*.apinotes;*.ifs;" , "YAML Files" , NULL , InitTabPageControlsAfterLoadFile_YAML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_YAML_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_MAKEFILE , "doctype_MAKEFILE.conf" , ";makefile;Makefile;Makefile.gcc;*.mk;*.mak;configure;" , "Makefile Files" , NULL , InitTabPageControlsAfterLoadFile_MAKEFILE , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_MAKEFILE_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CMAKE , "doctype_CMAKE.conf" , ";CMakeLists.txt;*.cmake;*.cmake.in;*.ctest;*.ctest.in;" , "CMake Files" , NULL , InitTabPageControlsAfterLoadFile_CMAKE , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CMAKE_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_MARKDOWN , "doctype_MARKDOWN.conf" , ";*.md;*.markdown;" , "Markdown Files" , NULL , InitTabPageControlsAfterLoadFile_MARKDOWN , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_MARKDOWN_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LOG , "doctype_LOG.conf" , ";*.log;" , "Log Files" , NULL , InitTabPageControlsAfterLoadFile_LOG , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_END }
	} ;

struct DocTypeConfig *GetDocTypeConfig( char *pcExtname )
{
	char			acExtname[ 1+_MAX_EXT+1 ] ;
	struct DocTypeConfig	*pstDocTypeConfig = NULL ;

	memset( acExtname , 0x00 , sizeof(acExtname) );
	_snprintf( acExtname , sizeof(acExtname)-1 , ";*%s;" , pcExtname );
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; pstDocTypeConfig++ )
	{
		if( strstr( pstDocTypeConfig->pcFileExtnames , acExtname ) )
			return pstDocTypeConfig;
	}

	return NULL;
}

#define ALL_SUPPORT_FILES_TYPEDESC_S	"All Support Files"
#define ALL_FILES_TYPEDESC_S		"All Files"
#define ALL_FILES_EXTNAMES_S		"*.*"

char *GetFileDialogFilterPtr()
{
	size_t			nFileDialogFilterSize ;
	struct DocTypeConfig	*pstDocTypeConfig = NULL ;
	static char		*acFileDialogFilter = NULL ;
	char			*p = NULL ;

	if( acFileDialogFilter )
		return acFileDialogFilter;

	nFileDialogFilterSize = strlen(ALL_SUPPORT_FILES_TYPEDESC_S)+1+1+1 + 1 + 1 ;
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; pstDocTypeConfig++ )
	{
		nFileDialogFilterSize += strlen(pstDocTypeConfig->pcFileExtnames+1) + strlen(pstDocTypeConfig->pcFileExtnames+1) ;
	}
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->pcFileExtnames ; pstDocTypeConfig++ )
	{
		nFileDialogFilterSize += strlen(pstDocTypeConfig->pcFileTypeDesc)+1+1+(strlen(pstDocTypeConfig->pcFileExtnames)-2)+1 + 1 + (strlen(pstDocTypeConfig->pcFileExtnames)-2) + 1 ;
	}
	nFileDialogFilterSize += strlen(ALL_FILES_TYPEDESC_S)+1+1+strlen(ALL_FILES_EXTNAMES_S)+1 + 1 + strlen(ALL_FILES_EXTNAMES_S) + 1 ;

	nFileDialogFilterSize++;

	acFileDialogFilter = (char*)malloc( nFileDialogFilterSize ) ;
	if( acFileDialogFilter == NULL )
		return NULL;
	memset( acFileDialogFilter , 0x00 , nFileDialogFilterSize );

	p = acFileDialogFilter ;
	p += sprintf( p , "%s (" , ALL_SUPPORT_FILES_TYPEDESC_S ) ;
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; pstDocTypeConfig++ )
	{
		p += sprintf( p , "%s" , pstDocTypeConfig->pcFileExtnames+1 ) ;
	}
	p += sprintf( p , ")" ) ;
	p++;
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; pstDocTypeConfig++ )
	{
		p += sprintf( p , "%s" , pstDocTypeConfig->pcFileExtnames+1 ) ;
	}
	p++;
	for( pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; pstDocTypeConfig++ )
	{
		p += sprintf( p , "%s (%.*s)" , pstDocTypeConfig->pcFileTypeDesc , (int)strlen(pstDocTypeConfig->pcFileExtnames)-2 , pstDocTypeConfig->pcFileExtnames+1 ) ;
		p++;
		p += sprintf( p , "%.*s" , (int)strlen(pstDocTypeConfig->pcFileExtnames)-2 , pstDocTypeConfig->pcFileExtnames+1 ) ;
		p++;
	}
	p += sprintf( p , "%s (%s)" , ALL_FILES_TYPEDESC_S , ALL_FILES_EXTNAMES_S ) ;
	p++;
	p += sprintf( p , "%s" , ALL_FILES_EXTNAMES_S ) ;
	p++;

	p++;

	if( p-acFileDialogFilter != nFileDialogFilterSize )
		return NULL;

	return acFileDialogFilter;
}

int BuildAutoCompletedShowTree( struct DocTypeConfig *pstFileExtnameMapper , char *word2 )
{
	char				*acWord2Dup = NULL ;
	char				*p1 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode = NULL ;

	acWord2Dup = _strdup(word2) ;
	if( acWord2Dup == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成单词临时缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -4;
	}

	p1 = strtok( acWord2Dup , " " ) ;
	while( p1 )
	{
		pstAutoCompletedShowNode = (struct AutoCompletedShowNode *)malloc( sizeof(struct AutoCompletedShowNode) ) ;
		if( pstAutoCompletedShowNode == NULL )
		{
			::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWord2Dup );
			return -4;
		}
		memset( pstAutoCompletedShowNode , 0x00 , sizeof(struct AutoCompletedShowNode) );

		pstAutoCompletedShowNode->acAutoCompletedString = _strdup( p1 ) ;

		pstFileExtnameMapper->sAutoCompletedBufferSize += strlen(pstAutoCompletedShowNode->acAutoCompletedString)+1 ;

		LinkAutoCompletedShowTreeNode( & (pstFileExtnameMapper->stAutoCompletedShowTree) , pstAutoCompletedShowNode );

		p1 = strtok( NULL , " " ) ;
	}

	pstFileExtnameMapper->sAutoCompletedBufferSize++;

	free( acWord2Dup );

	return 0;
}

int ExpandAutoCompletedShowTreeToBuffer( struct DocTypeConfig *pstFileExtnameMapper )
{
	if( pstFileExtnameMapper->acAutoCompletedBuffer )
		free( pstFileExtnameMapper->acAutoCompletedBuffer );
	pstFileExtnameMapper->acAutoCompletedBuffer = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize ) ;
	if( pstFileExtnameMapper->acAutoCompletedBuffer == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedBuffer , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	if( pstFileExtnameMapper->acAutoCompletedShowBuffer )
		free( pstFileExtnameMapper->acAutoCompletedShowBuffer );
	pstFileExtnameMapper->acAutoCompletedShowBuffer = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize ) ;
	if( pstFileExtnameMapper->acAutoCompletedShowBuffer == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedShowBuffer , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	struct rb_node			*nodeTravel = NULL ;
	struct rb_node			*nodeLast = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode = NULL ;
	char				*p = pstFileExtnameMapper->acAutoCompletedBuffer ;
	int				offset = 0 ;
	int				len ;

	nodeTravel = rb_first( & (pstFileExtnameMapper->stAutoCompletedShowTree.treeAutoCompletedShow) ) ;
	nodeLast = rb_last( & (pstFileExtnameMapper->stAutoCompletedShowTree.treeAutoCompletedShow) ) ;
	while( nodeTravel )
	{
		pstAutoCompletedShowNode = rb_entry( nodeTravel , struct AutoCompletedShowNode , nodeAutoCompletedShow ) ;

		len = sprintf( p , "%s " , pstAutoCompletedShowNode->acAutoCompletedString );

		pstAutoCompletedShowNode->begin = offset ;
		pstAutoCompletedShowNode->end = offset + len ;

		p += len ;
		offset += len ;

		if( nodeTravel == nodeLast )
			break;
		nodeTravel = rb_next( nodeTravel ) ;
	}

	return 0;
}

int BuildAutoCompletedShowTree2( struct DocTypeConfig *pstFileExtnameMapper , char *word2 )
{
	char				*acWord2Dup2 = NULL ;
	char				*p1 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode2 = NULL ;

	acWord2Dup2 = _strdup(word2) ;
	if( acWord2Dup2 == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成单词临时缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -4;
	}

	p1 = strtok( acWord2Dup2 , " " ) ;
	while( p1 )
	{
		pstAutoCompletedShowNode2 = (struct AutoCompletedShowNode *)malloc( sizeof(struct AutoCompletedShowNode) ) ;
		if( pstAutoCompletedShowNode2 == NULL )
		{
			::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWord2Dup2 );
			return -4;
		}
		memset( pstAutoCompletedShowNode2 , 0x00 , sizeof(struct AutoCompletedShowNode) );

		pstAutoCompletedShowNode2->acAutoCompletedString = _strdup( p1 ) ;

		pstFileExtnameMapper->sAutoCompletedBufferSize2 += strlen(pstAutoCompletedShowNode2->acAutoCompletedString)+1 ;

		LinkAutoCompletedShowTreeNode( & (pstFileExtnameMapper->stAutoCompletedShowTree2) , pstAutoCompletedShowNode2 );

		p1 = strtok( NULL , " " ) ;
	}

	pstFileExtnameMapper->sAutoCompletedBufferSize2++;

	free( acWord2Dup2 );

	return 0;
}

int ExpandAutoCompletedShowTreeToBuffer2( struct DocTypeConfig *pstFileExtnameMapper )
{
	if( pstFileExtnameMapper->acAutoCompletedBuffer2 )
		free( pstFileExtnameMapper->acAutoCompletedBuffer2 );
	pstFileExtnameMapper->acAutoCompletedBuffer2 = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize2 ) ;
	if( pstFileExtnameMapper->acAutoCompletedBuffer2 == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedBuffer2 , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	if( pstFileExtnameMapper->acAutoCompletedShowBuffer2 )
		free( pstFileExtnameMapper->acAutoCompletedShowBuffer2 );
	pstFileExtnameMapper->acAutoCompletedShowBuffer2 = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize2 ) ;
	if( pstFileExtnameMapper->acAutoCompletedShowBuffer2 == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedShowBuffer2 , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize2 );

	struct rb_node			*nodeTravel2 = NULL ;
	struct rb_node			*nodeLast2 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode2 = NULL ;
	char				*p = pstFileExtnameMapper->acAutoCompletedBuffer2 ;
	int				offset = 0 ;
	int				len ;

	nodeTravel2 = rb_first( & (pstFileExtnameMapper->stAutoCompletedShowTree2.treeAutoCompletedShow) ) ;
	nodeLast2 = rb_last( & (pstFileExtnameMapper->stAutoCompletedShowTree2.treeAutoCompletedShow) ) ;
	while( nodeTravel2 )
	{
		pstAutoCompletedShowNode2 = rb_entry( nodeTravel2 , struct AutoCompletedShowNode , nodeAutoCompletedShow ) ;

		len = sprintf( p , "%s " , pstAutoCompletedShowNode2->acAutoCompletedString );

		pstAutoCompletedShowNode2->begin = offset ;
		pstAutoCompletedShowNode2->end = offset + len ;

		p += len ;
		offset += len ;

		if( nodeTravel2 == nodeLast2 )
			break;
		nodeTravel2 = rb_next( nodeTravel2 ) ;
	}

	return 0;
}

int LoadLexerConfigFile( const char *filename , struct DocTypeConfig *pstFileExtnameMapper , char *filebuf )
{
	char				pathfilename[ MAX_PATH ] ;
	FILE				*fp = NULL ;
	char				*pcItemName = NULL ;
	char				*pcItemValue = NULL ;
	char				*p1 = NULL ;
	char				*pcResult = NULL ;
	struct CallTipShowNode		*pstCallTipShowNode = NULL ;
	int				nret = 0 ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\%s" , g_acModulePathName , pstFileExtnameMapper->pcFileTypeConfigFilename );
	fp = fopen( pathfilename , "r" ) ;
	if( fp == NULL )
		return 1;

	while(1)
	{
		memset( filebuf , 0x00 , FILEBUF_MAX );
		if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
			break;

		nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
		if( nret > 0 )
			continue;
		else if( nret < 0 )
			return nret;

		if( strcmp( pcItemName , "keywords.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords2.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords2 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords2 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords3.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords3 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords3 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords4.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords4 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords4 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords5.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords5 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords5 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords6.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords6 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords6 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "autocomplete.set" ) == 0 )
		{
			pstFileExtnameMapper->autocomplete_set = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->autocomplete_set == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存用以存放autocomplete.set"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			DestroyAutoCompletedShowTree( & (pstFileExtnameMapper->stAutoCompletedShowTree) );
			pstFileExtnameMapper->sAutoCompletedBufferSize = 0 ;

			nret = BuildAutoCompletedShowTree( pstFileExtnameMapper , pstFileExtnameMapper->autocomplete_set ) ;
			if( nret )
				return nret;

			nret = ExpandAutoCompletedShowTreeToBuffer( pstFileExtnameMapper ) ;
			if( nret )
				return nret;
		}
		else if( strcmp( pcItemName , "autocomplete2.set" ) == 0 )
		{
			pstFileExtnameMapper->autocomplete2_set = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->autocomplete2_set == NULL )
			{
				::MessageBox(NULL, TEXT("不能分配内存用以存放autocomplete.set"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			DestroyAutoCompletedShowTree( & (pstFileExtnameMapper->stAutoCompletedShowTree2) );
			pstFileExtnameMapper->sAutoCompletedBufferSize2 = 0 ;

			nret = BuildAutoCompletedShowTree2( pstFileExtnameMapper , pstFileExtnameMapper->autocomplete2_set ) ;
			if( nret )
				return nret;

			nret = ExpandAutoCompletedShowTreeToBuffer2( pstFileExtnameMapper ) ;
			if( nret )
				return nret;
		}
		else if( strcmp( pcItemName , "calltip.add" ) == 0 )
		{
			pstCallTipShowNode = (struct CallTipShowNode *)malloc( sizeof(struct CallTipShowNode) ) ;
			if( pstCallTipShowNode == NULL )
				return -4;
			memset( pstCallTipShowNode , 0x00 , sizeof(struct CallTipShowNode) );

			p1 = strchr( pcItemValue , '|' ) ;
			if( p1 == NULL )
				continue;

			(*p1) = '\0' ;
			pstCallTipShowNode->acCallTipFuncName = _strdup( pcItemValue ) ;
			pstCallTipShowNode->acCallTipFuncDesc = _strdup( p1+1 ) ;
			FoldNewLineString( pstCallTipShowNode->acCallTipFuncDesc );

			LinkCallTipShowTreeNode( & (pstFileExtnameMapper->stCallTipShowTree) , pstCallTipShowNode );
		}
		else if( strcmp( pcItemName , "symbol_reqular_exp" ) == 0 )
		{
			pstFileExtnameMapper->acSymbolReqularExp = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->acSymbolReqularExp == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "file.extnames" ) == 0 )
		{
			pstFileExtnameMapper->pcFileExtnames = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->pcFileExtnames == NULL )
				return -3;
		}
	}

	return 0;
}

int BeginReloadSymbolListOrTreeThread()
{
	if( ReloadSymbolListOrTreeThreadHandler == NULL )
	{
		ReloadSymbolListOrTreeThreadHandler = _beginthread( ReloadSymbolListOrTreeThreadEntry , 0 , NULL ) ;
		if( ReloadSymbolListOrTreeThreadHandler == -1L )
		{
			ReloadSymbolListOrTreeThreadHandler = NULL ;
			ErrorBox( "创建定时刷新符号列表或符号树线程失败" );
			return -1;
		}
	}

	return 0;
}

void ReloadSymbolListOrTreeThreadEntry( void *param )
{
	int		nret = 0 ;

	while(1)
	{
		HANDLE mutex = ::CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
		if( mutex )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig )
			{
				if( g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
				{
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( g_pnodeCurrentTabPage );
				}

				if( g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
				{
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( g_pnodeCurrentTabPage );
				}

				if( g_pnodeCurrentTabPage->pstDocTypeConfig->nDocType == DOCTYPE_SQL
					|| g_pnodeCurrentTabPage->pstDocTypeConfig->nDocType == DOCTYPE_XML
					|| g_pnodeCurrentTabPage->pstDocTypeConfig->nDocType == DOCTYPE_JSON )
				{
					CloseHandle(mutex);
					break;
				}
			}

			CloseHandle(mutex);
		}

		if( g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval <= 0 )
			break;

		Sleep( g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval*1000 );
	}

	ReloadSymbolListOrTreeThreadHandler = NULL ;
	_endthread();
}

void UpdateFileTypeMenu()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_VIEW ;
	HMENU		hMenu_FILETYPE ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = ::GetMenu(g_hwndMainWindow) ;
	hMenu_VIEW = ::GetSubMenu( hRootMenu , 3 ) ;
	hMenu_FILETYPE = ::GetSubMenu( hMenu_VIEW , 2 ) ;
	count = ::GetMenuItemCount( hMenu_FILETYPE ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = ::DeleteMenu( hMenu_FILETYPE , 0 , MF_BYPOSITION );
	}

	struct DocTypeConfig	*pstDocTypeConfig = NULL ;

	for( index = 0 , pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; index++ , pstDocTypeConfig++ )
	{
		bret = ::AppendMenu( hMenu_FILETYPE , MF_POPUP|MF_STRING , IDM_VIEW_SWITCH_FILETYPE_BASE+index , pstDocTypeConfig->pcFileTypeDesc );
	}

	return;
}
