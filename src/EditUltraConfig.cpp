#include "framework.h"

void SetEditUltraMainConfigDefault( struct EditUltraMainConfig *pstEditUltraMainConfig )
{
	pstEditUltraMainConfig->bCreateNewFileOnNewBoot = FALSE ;
	pstEditUltraMainConfig->bOpenFilesThatOpenningOnExit = FALSE ;
	pstEditUltraMainConfig->bSetReadOnlyAfterOpenFile = FALSE ;
	memset( pstEditUltraMainConfig->aacOpenPathFilenameRecently , 0x00 , sizeof(pstEditUltraMainConfig->aacOpenPathFilenameRecently) );

	pstEditUltraMainConfig->bEnableAutoAddCloseChar = TRUE ;
	pstEditUltraMainConfig->bEnableAutoIdentation = TRUE ;
	pstEditUltraMainConfig->bCheckUpdateWhereSelectTabPage = FALSE ;
	pstEditUltraMainConfig->bPromptWhereAutoUpdate = TRUE ;
	pstEditUltraMainConfig->nNewFileEols = 0 ;
	pstEditUltraMainConfig->nNewFileEncoding = 65001 ;

	pstEditUltraMainConfig->bLineNumberVisiable = TRUE ;
	pstEditUltraMainConfig->bBookmarkVisiable = TRUE ;
	pstEditUltraMainConfig->bWhiteSpaceVisiable = FALSE ;
	pstEditUltraMainConfig->nWhiteSpaceSize = WHITESPACE_SIZE ;
	pstEditUltraMainConfig->bNewLineVisiable = FALSE ;
	pstEditUltraMainConfig->bIndentationGuidesVisiable = TRUE ;

	pstEditUltraMainConfig->bBlockFoldVisiable = TRUE ;

	strcpy( pstEditUltraMainConfig->acProcessFileCommand , "\"C:\\Windows\\notepad.exe\" \"%F\"" );
	strcpy( pstEditUltraMainConfig->acProcessTextCommand , "\"C:\\Program Files\\Mozilla Firefox\\firefox.exe\" \"https://www.baidu.com/s?wd=%T\"" );

	pstEditUltraMainConfig->nFileTreeBarWidth = FILETREEBAR_WIDTH_DEFAULT ;
	pstEditUltraMainConfig->nTabWidth = EDITOR_TAB_WIDTH_DEFAULT ;
	pstEditUltraMainConfig->bWrapLineMode = TRUE ;
	pstEditUltraMainConfig->nSymbolListWidth = SYMBOLLIST_WIDTH_DEFAULT ;
	pstEditUltraMainConfig->nSymbolTreeWidth = SYMBOLTREE_WIDTH_DEFAULT ;
	pstEditUltraMainConfig->nSqlQueryResultEditHeight = SQLQUERYRESULT_EDIT_HEIGHT_DEFAULT ;
	pstEditUltraMainConfig->nSqlQueryResultListViewHeight = SQLQUERYRESULT_LISTVIEW_HEIGHT_DEFAULT ;

	pstEditUltraMainConfig->bEnableAutoCompletedShow = TRUE ;
	pstEditUltraMainConfig->nAutoCompletedShowAfterInputCharacters = 1 ;
	pstEditUltraMainConfig->bEnableCallTipShow = TRUE ;

	return;
}

int LoadMainConfigFile( char *filebuf )
{
	char				pathfilename[ MAX_PATH ] ;
	FILE				*fp = NULL ;
	char				*pcItemName = NULL ;
	char				*pcItemValue = NULL ;
	char				*p1 = NULL ;
	char				*pcResult = NULL ;
	struct CallTipShowNode		*pstCallTipShowNode = NULL ;
	int				nret = 0 ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\eux.conf" , g_acModulePathName );
	fp = fopen( pathfilename , "r" ) ;
	if( fp == NULL )
		return -2;

	while(1)
	{
		memset( filebuf , 0x00 , FILEBUF_MAX );
		if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
			break;

		nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
		if( nret > 0 )
			continue;
		else if( nret < 0 )
			return nret;

		if( strcmp( pcItemName , "create_newfile_on_newboot" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bCreateNewFileOnNewBoot = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bCreateNewFileOnNewBoot = FALSE ;
		}
		else if( strcmp( pcItemName , "openfiles_that_openning_on_exit" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit = FALSE ;
		}
		else if( strncmp( pcItemName , "openfile_on_boot_" , 17 ) == 0 )
		{
			int	index = atoi(pcItemName+17) ;

			strncpy( g_stEditUltraMainConfig.aacOpenFilesOnBoot[index%OPENFILES_ON_BOOT_MAXCOUNT] , pcItemValue , sizeof(g_stEditUltraMainConfig.aacOpenFilesOnBoot[index%OPENFILES_ON_BOOT_MAXCOUNT])-1 );
		}
		else if( strcmp( pcItemName , "set_read_only_after_open_file" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile = FALSE ;
		}
		else if( strncmp( pcItemName , "open_pathfilename_recently_" , 27 ) == 0 )
		{
			int	index = atoi(pcItemName+27) ;

			strncpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index%OPEN_PATHFILENAME_RECENTLY_MAXCOUNT] , pcItemValue , sizeof(g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index%OPEN_PATHFILENAME_RECENTLY_MAXCOUNT])-1 );
		}
		else if( strcmp( pcItemName , "check_update_where_selecttabpage" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage = FALSE ;
		}
		else if( strcmp( pcItemName , "prompt_where_autoupdate" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bPromptWhereAutoUpdate = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bPromptWhereAutoUpdate = FALSE ;
		}
		else if( strcmp( pcItemName , "newfile_eols" ) == 0 )
		{
			g_stEditUltraMainConfig.nNewFileEols = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "newfile_encoding" ) == 0 )
		{
			g_stEditUltraMainConfig.nNewFileEncoding = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "enable_auto_add_close_char" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoAddCloseChar = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoAddCloseChar = FALSE ;
		}
		else if( strcmp( pcItemName , "enable_auto_identation" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoIdentation = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoIdentation = FALSE ;
		}
		else if( strcmp( pcItemName , "window_theme" ) == 0 )
		{
			strncpy( g_stEditUltraMainConfig.acWindowTheme , pcItemValue , sizeof(g_stEditUltraMainConfig.acWindowTheme)-1 );
		}
		else if( strcmp( pcItemName , "tab_width" ) == 0 )
		{
			g_stEditUltraMainConfig.nTabWidth = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "onkeydown_tab_convert_spaces" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces = FALSE ;
		}
		else if( strcmp( pcItemName , "wrapline_mode" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bWrapLineMode = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bWrapLineMode = FALSE ;
		}
		else if( strcmp( pcItemName , "line_number_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bLineNumberVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bLineNumberVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "bookmark_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bBookmarkVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bBookmarkVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "white_space_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bWhiteSpaceVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bWhiteSpaceVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "white_space_size" ) == 0 )
		{
			g_stEditUltraMainConfig.nWhiteSpaceSize = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "newline_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bNewLineVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bNewLineVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "indentation_guides_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bIndentationGuidesVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bIndentationGuidesVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "file_treebar_width" ) == 0 )
		{
			g_stEditUltraMainConfig.nFileTreeBarWidth = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "symbol_list_width" ) == 0 )
		{
			g_stEditUltraMainConfig.nSymbolListWidth = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "symbol_tree_width" ) == 0 )
		{
			g_stEditUltraMainConfig.nSymbolTreeWidth = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "sqlquery_result_edit_height" ) == 0 )
		{
			g_stEditUltraMainConfig.nSqlQueryResultEditHeight = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "sqlquery_result_listview_height" ) == 0 )
		{
			g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "reload_symbollis_or_tree_interval" ) == 0 )
		{
			g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "block_fold_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bBlockFoldVisiable = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bBlockFoldVisiable = FALSE ;
		}
		else if( strcmp( pcItemName , "enable_auto_completed_show" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoCompletedShow = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bEnableAutoCompletedShow = FALSE ;
		}
		else if( strcmp( pcItemName , "auto_completed_show_after_input_characters" ) == 0 )
		{
			g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "enable_call_tip_show" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				g_stEditUltraMainConfig.bEnableCallTipShow = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				g_stEditUltraMainConfig.bEnableCallTipShow = FALSE ;
		}
		else if( strcmp( pcItemName , "process_file_command" ) == 0 )
		{
			strncpy( g_stEditUltraMainConfig.acProcessFileCommand , pcItemValue , sizeof(g_stEditUltraMainConfig.acProcessFileCommand)-1 );
		}
		else if( strcmp( pcItemName , "process_text_command" ) == 0 )
		{
			strncpy( g_stEditUltraMainConfig.acProcessTextCommand , pcItemValue , sizeof(g_stEditUltraMainConfig.acProcessTextCommand)-1 );
		}
	}

	return 0;
}

void FoldConfigStringItemValue( char *value )
{
	char	*p = NULL ;

	for( p = value ; *p ; p++ )
	{
		if( *p == '\\' && *(p+1) )
		{
			memmove( p , p+1 , strlen(p+1)+1 );
		}
	}

	return;
}

int DecomposeConfigFileBuffer( char *filebuf , char **ppcItemName , char **ppcItemValue )
{
	size_t		filebuf_len ;
	char		*filebuf_end = NULL ;
	char		*p1 = NULL ;
	char		*p2 = NULL ;
	char		*pcItemName = NULL ;
	char		*pcItemValue = NULL ;

	filebuf_len = strlen(filebuf) ;
	filebuf_end = filebuf + filebuf_len - 1 ;
	while( filebuf_end >= filebuf && strchr( " \t\r\n" , (*filebuf_end) ) )
	{
		filebuf_end--;
	}
	*(filebuf_end+1) = '\0' ;

	pcItemName = filebuf ;
	while( (*pcItemName) && strchr( " \t" , (*pcItemName) ) )
		pcItemName++;
	if( (*pcItemName) == '\0' )
		return 1;
	if( (*pcItemName) == '#' )
		return 1;

	p1 = pcItemName ;
	while( (*p1) && ! strchr( " \t=" , (*p1) ) )
		p1++;
	if( (*p1) == '\0' )
		return 1;

	p2 = p1 ;
	while( (*p2) && (*p2) != '=' )
		p2++;
	if( (*p2) == '\0' )
		return 1;

	pcItemValue = p2 + 1 ;
	while( (*pcItemValue) && strchr( " \t" , (*pcItemValue) ) )
		pcItemValue++;

	(*p1) = '\0' ;

	if( (*pcItemValue) == '"' )
	{
		pcItemValue++;
		for( p2 = pcItemValue ; *(p2) ; p2++ )
		{
			if( *(p2) == '\\' )
			{
				p2++;
				if( *(p2) == '\0' )
					break;
			}
			else if( *(p2) == '"' )
			{
				break;
			}
		}
		if( *(p2) == '\0' )
			return -1;

		*(p2) = '\0' ;
		FoldConfigStringItemValue( pcItemValue );
	}

	if( ppcItemName )
		(*ppcItemName) = pcItemName ;
	if( ppcItemValue )
		(*ppcItemValue) = pcItemValue ;

	return 0;
}

int LoadConfig()
{
	char	*filebuf = NULL ;
	int	nret = 0 ;

	GenarateConfigKey();

	filebuf = (char*)malloc( FILEBUF_MAX ) ;
	if( filebuf == NULL )
	{
		::MessageBox(NULL, TEXT("�����ڴ��Դ�������ļ�������ʧ��"), TEXT("����"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nret = LoadMainConfigFile( filebuf ) ;
	if( nret )
	{
		::MessageBox(NULL, TEXT("װ��������ʧ�ܣ�ʹ��ȱʡ����"), TEXT("����"), MB_ICONERROR | MB_OK);
	}

	nret = LoadAllStyleThemeConfigFiles( filebuf ) ;
	if( nret )
	{
		::MessageBox(NULL, TEXT("װ�ط����������ʧ�ܣ�ʹ��ȱʡ����"), TEXT("����"), MB_ICONERROR | MB_OK);
	}

	for( struct DocTypeConfig *pstFileExtnameMapper = g_astDocTypeConfig ; pstFileExtnameMapper->pcFileExtnames ; pstFileExtnameMapper++ )
	{
		if( pstFileExtnameMapper->pcFileTypeConfigFilename == NULL )
			continue;
		
		nret = LoadLexerConfigFile( pstFileExtnameMapper->pcFileTypeConfigFilename , pstFileExtnameMapper , filebuf ) ;
		if( nret < 0 )
		{
			free( filebuf );
			return -1;
		}
	}

	nret = LoadRemoteFileServersConfigFromFile( filebuf ) ;
	if( nret < 0 )
	{
		::MessageBox(NULL, TEXT("װ��Զ���ļ�����������ʧ��"), TEXT("����"), MB_ICONERROR | MB_OK);
		free( filebuf );
		return -1;
	}

	free( filebuf );

	return 0;
}

void ExpandConfigStringItemValue( FILE *fp , char *value )
{
	char	*p = NULL ;

	fprintf( fp , "\"" );

	for( p = value ; *p ; p++ )
	{
		if( *p == '"' || *p == '\\' )
			fputc( '\\' , fp );
		fputc( *p , fp );
	}
	fprintf( fp , "\"\n" );

	return;
}

int SaveMainConfigFile()
{
	char		pathfilename[ MAX_PATH ] ;
	FILE		*fp = NULL ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\eux.conf" , g_acModulePathName );
	fp = fopen( pathfilename , "w" ) ;
	if( fp == NULL )
		return -1;

	if( g_stEditUltraMainConfig.bCreateNewFileOnNewBoot == TRUE )
		fprintf( fp , "create_newfile_on_newboot = TRUE\n" );
	else if( g_stEditUltraMainConfig.bCreateNewFileOnNewBoot == FALSE )
		fprintf( fp , "create_newfile_on_newboot = FALSE\n" );
	else
		fprintf( fp , "create_newfile_on_newboot = FALSE\n" );

	if( g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == TRUE )
		fprintf( fp , "openfiles_that_openning_on_exit = TRUE\n" );
	else if( g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == FALSE )
		fprintf( fp , "openfiles_that_openning_on_exit = FALSE\n" );
	else
		fprintf( fp , "openfiles_that_openning_on_exit = FALSE\n" );

	for( int index = 0 ; g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == TRUE && index < OPENFILES_ON_BOOT_MAXCOUNT ; index++ )
	{
		if( g_stEditUltraMainConfig.aacOpenFilesOnBoot[index][0] == '\0' )
			break;

		fprintf( fp , "openfile_on_boot_%d = " , index ); ExpandConfigStringItemValue( fp , g_stEditUltraMainConfig.aacOpenFilesOnBoot[index] );
	}

	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == TRUE )
		fprintf( fp , "set_read_only_after_open_file = TRUE\n" );
	else if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == FALSE )
		fprintf( fp , "set_read_only_after_open_file = FALSE\n" );
	else
		fprintf( fp , "set_read_only_after_open_file = FALSE\n" );

	for( int index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index][0] == '\0' )
			break;

		fprintf( fp , "open_pathfilename_recently_%d = " , index ); ExpandConfigStringItemValue( fp , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}

	if( g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage == TRUE )
		fprintf( fp , "check_update_where_selecttabpage = TRUE\n" );
	else if( g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage == FALSE )
		fprintf( fp , "check_update_where_selecttabpage = FALSE\n" );
	else
		fprintf( fp , "check_update_where_selecttabpage = TRUE\n" );

	if( g_stEditUltraMainConfig.bPromptWhereAutoUpdate == TRUE )
		fprintf( fp , "prompt_where_autoupdate = TRUE\n" );
	else if( g_stEditUltraMainConfig.bPromptWhereAutoUpdate == FALSE )
		fprintf( fp , "prompt_where_autoupdate = FALSE\n" );
	else
		fprintf( fp , "prompt_where_autoupdate = TRUE\n" );

	fprintf( fp , "newfile_eols = %d\n" , g_stEditUltraMainConfig.nNewFileEols );

	fprintf( fp , "newfile_encoding = %d\n" , g_stEditUltraMainConfig.nNewFileEncoding );

	if( g_stEditUltraMainConfig.bEnableAutoAddCloseChar == TRUE )
		fprintf( fp , "enable_auto_add_close_char = TRUE\n" );
	else if( g_stEditUltraMainConfig.bEnableAutoAddCloseChar == FALSE )
		fprintf( fp , "enable_auto_add_close_char = FALSE\n" );
	else
		fprintf( fp , "enable_auto_add_close_char = TRUE\n" );

	if( g_stEditUltraMainConfig.bEnableAutoIdentation == TRUE )
		fprintf( fp , "enable_auto_identation = TRUE\n" );
	else if( g_stEditUltraMainConfig.bEnableAutoIdentation == FALSE )
		fprintf( fp , "enable_auto_identation = FALSE\n" );
	else
		fprintf( fp , "enable_auto_identation = TRUE\n" );

	fprintf( fp , "window_theme = %s\n" , g_stEditUltraMainConfig.acWindowTheme );

	fprintf( fp , "tab_width = %d\n" , g_stEditUltraMainConfig.nTabWidth );
	if( g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces == TRUE )
		fprintf( fp , "onkeydown_tab_convert_spaces = TRUE\n" );
	else if( g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces == FALSE )
		fprintf( fp , "onkeydown_tab_convert_spaces = FALSE\n" );
	else
		fprintf( fp , "onkeydown_tab_convert_spaces = FALSE\n" );

	if( g_stEditUltraMainConfig.bWrapLineMode == TRUE )
		fprintf( fp , "wrapline_mode = TRUE\n" );
	else if( g_stEditUltraMainConfig.bWrapLineMode == FALSE )
		fprintf( fp , "wrapline_mode = FALSE\n" );
	else
		fprintf( fp , "wrapline_mode = TRUE\n" );

	if( g_stEditUltraMainConfig.bLineNumberVisiable == TRUE )
		fprintf( fp , "line_number_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bLineNumberVisiable == FALSE )
		fprintf( fp , "line_number_visiable = FALSE\n" );
	else
		fprintf( fp , "line_number_visiable = TRUE\n" );

	if( g_stEditUltraMainConfig.bBookmarkVisiable == TRUE )
		fprintf( fp , "bookmark_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bBookmarkVisiable == FALSE )
		fprintf( fp , "bookmark_visiable = FALSE\n" );
	else
		fprintf( fp , "bookmark_visiable = TRUE\n" );

	if( g_stEditUltraMainConfig.bWhiteSpaceVisiable == TRUE )
		fprintf( fp , "white_space_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bWhiteSpaceVisiable == FALSE )
		fprintf( fp , "white_space_visiable = FALSE\n" );
	else
		fprintf( fp , "white_space_visiable = FALSE\n" );

	if( g_stEditUltraMainConfig.bNewLineVisiable == TRUE )
		fprintf( fp , "newline_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bNewLineVisiable == FALSE )
		fprintf( fp , "newline_visiable = FALSE\n" );
	else
		fprintf( fp , "newline_visiable = FALSE\n" );

	if( g_stEditUltraMainConfig.bIndentationGuidesVisiable == TRUE )
		fprintf( fp , "indentation_guides_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bIndentationGuidesVisiable == FALSE )
		fprintf( fp , "indentation_guides_visiable = FALSE\n" );
	else
		fprintf( fp , "indentation_guides_visiable = TRUE\n" );

	fprintf( fp , "white_space_size = %d\n" , g_stEditUltraMainConfig.nWhiteSpaceSize );

	fprintf( fp , "file_treebar_width = %d\n" , g_stEditUltraMainConfig.nFileTreeBarWidth );
	fprintf( fp , "symbol_list_width = %d\n" , g_stEditUltraMainConfig.nSymbolListWidth );
	fprintf( fp , "symbol_tree_width = %d\n" , g_stEditUltraMainConfig.nSymbolTreeWidth );
	fprintf( fp , "sqlquery_result_edit_height = %d\n" , g_stEditUltraMainConfig.nSqlQueryResultEditHeight );
	fprintf( fp , "sqlquery_result_listview_height = %d\n" , g_stEditUltraMainConfig.nSqlQueryResultListViewHeight );

	fprintf( fp , "reload_symbollis_or_tree_interval = %d\n" , g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval );
	if( g_stEditUltraMainConfig.bBlockFoldVisiable == TRUE )
		fprintf( fp , "block_fold_visiable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bBlockFoldVisiable == FALSE )
		fprintf( fp , "block_fold_visiable = FALSE\n" );
	else
		fprintf( fp , "block_fold_visiable = TRUE\n" );
	if( g_stEditUltraMainConfig.bEnableAutoCompletedShow == TRUE )
		fprintf( fp , "auto_completed_show_enable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bEnableAutoCompletedShow == FALSE )
		fprintf( fp , "auto_completed_show_enable = FALSE\n" );
	else
		fprintf( fp , "auto_completed_show_enable = TRUE\n" );
	fprintf( fp , "auto_completed_show_after_input_characters = %d\n" , g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters );
	if( g_stEditUltraMainConfig.bEnableCallTipShow == TRUE )
		fprintf( fp , "call_tip_show_enable = TRUE\n" );
	else if( g_stEditUltraMainConfig.bEnableCallTipShow == FALSE )
		fprintf( fp , "call_tip_show_enable = FALSE\n" );
	else
		fprintf( fp , "call_tip_show_enable = TRUE\n" );

	fprintf( fp , "process_file_command = " ); ExpandConfigStringItemValue( fp , g_stEditUltraMainConfig.acProcessFileCommand );
	fprintf( fp , "process_text_command = " ); ExpandConfigStringItemValue( fp , g_stEditUltraMainConfig.acProcessTextCommand );

	fclose( fp );

	return 0;
}
