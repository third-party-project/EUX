﻿// EditUltra.cpp : 定义应用程序的入口点。
//

#include "framework.h"

#define MAX_LOADSTRING 100

struct EditUltraMainConfig		g_stEditUltraMainConfig ;

// 全局变量:
char		**g_argv = NULL ;
int		g_argc = 0 ;
HINSTANCE	g_hAppInstance = NULL ;				// 当前实例
HWND		g_hwndMainWindow = NULL ;			// 当前窗口
char		g_acAppName[100];
char		g_acWindowClassName[100];
WCHAR		g_szTitle[MAX_LOADSTRING];			// 标题栏文本
WCHAR		g_szWindowClass[MAX_LOADSTRING];		// 主窗口类名

/*
HWND		g_hwndToolBar_FileMenu = NULL ;
HWND		g_hwndToolBar_EditMenu = NULL ;
HWND		g_hwndReBar = NULL ;
int		g_nReBarHeight = 0 ;
*/
HWND		g_hwndToolBar = NULL ;
int		g_nToolBarHeight = 0 ;

HWND		g_hwndStatusBar = NULL ;
int		g_nStatusBarHeight = 0 ;

char		g_acModuleFileName[ MAX_PATH ] ;
char		g_acModulePathName[ MAX_PATH ] ;

int		g_nZoomReset = 0 ;

BOOL		g_bIsFileTreeBarHoverResizing = FALSE ;
BOOL		g_bIsFileTreeBarResizing = FALSE ;
BOOL		g_bIsTabPageMoving = FALSE ;
int		g_nTabPageMoveFrom = 0 ;
POINT		g_rectLMouseDown = { 0 } ;
BOOL		g_bIsFunctionListHoverResizing = FALSE ;
BOOL		g_bIsFunctionListResizing = FALSE ;
BOOL		g_bIsTreeViewHoverResizing = FALSE ;
BOOL		g_bIsTreeViewResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewResizing = FALSE ;

// 此代码模块中包含的函数的前向声明:
ATOM			MyRegisterClass(HINSTANCE hInstance);
BOOL			InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					 _In_opt_ HINSTANCE hPrevInstance,
					 _In_ LPWSTR	lpCmdLine,
					 _In_ int	nCmdShow)
{
	char	acCurrentDirectory[ MAX_PATH ] ;
	RECT	rectMainWindow ;

	int	nret = 0 ;

#if 0
	::MessageBox(NULL, "通过了", "通过了", MB_ICONERROR | MB_OK);
	exit(0);
#endif

#if 0
	/* test for test_SplitPathFilename */
	{
		test_SplitPathFilename();
		exit(0);
	}
#endif

#if 0
	/* test for GetApplicationByFileExt */
	{
		int nret = GetApplicationByFileExt(NULL,NULL) ;
		ConfirmErrorInfo("nret[%d]",nret);
		exit(0);


	}
#endif

#if 0
	/* test for SetApplicationByFileExt */
	{
		int nret = SetApplicationByFileExt(NULL,NULL) ;
		ConfirmErrorInfo("nret[%d]",nret);
		exit(0);


	}
#endif

	// ::MessageBoxW(NULL, lpCmdLine, NULL, MB_ICONERROR | MB_OK);

	memset( acCurrentDirectory , 0x00 , sizeof(acCurrentDirectory) );
	::GetCurrentDirectory( sizeof(acCurrentDirectory)-1 , acCurrentDirectory );
	SetLogcFile( (char*)"%s\\log\\EditUltra.log" , acCurrentDirectory );
	SetLogcLevel( LOGCLEVEL_NOLOG );
	DEBUGLOGC( "--- EditUltra Start Debugging ---" )

	UNREFERENCED_PARAMETER(hPrevInstance);
	g_argv = CommandLineToArgvA( lpCmdLine , & g_argc ) ;
#if 0
	{
		for( int i = 0 ; i < g_argc ; i++)
		{
			InfoBox( "[%d][%s]" , i , g_argv[i] );
		}
	}
#endif

	// 初始化全局字符串
	LoadStringA(hInstance, IDS_APP_TITLE, g_acAppName, 100);
	LoadStringA(hInstance, IDC_EUX, g_acWindowClassName, 100);
	LoadStringW(hInstance, IDS_APP_TITLE, g_szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_EUX, g_szWindowClass, MAX_LOADSTRING);

	{
		memset( g_acModuleFileName , 0x00 , sizeof(g_acModuleFileName) );
		::GetModuleFileName( NULL , g_acModuleFileName , sizeof(g_acModuleFileName) );
		strcpy( g_acModulePathName , g_acModuleFileName );
		char *p = strrchr( g_acModulePathName , '\\' ) ;
		if( p )
			*(p) = '\0' ;
	}

	HANDLE mutex = ::CreateMutex( NULL , TRUE , "Thank you , ualy" ) ;
	if( mutex == NULL )
	{
		ErrorBox( "创建互斥量失败[%d]" , ::GetLastError() );
		return -1;
	}

	if( g_argc == 1 && g_argv[0] )
	{
		HWND hwnd = ::FindWindow( g_acWindowClassName , NULL ) ;
		if( hwnd )
		{
			COPYDATASTRUCT	cpd ;

			memset( & cpd , 0x00 , sizeof(COPYDATASTRUCT) );
			cpd.lpData = g_argv[0] ;
			cpd.cbData = (DWORD)strlen(g_argv[0]) ;
			::SendMessage( hwnd , WM_COPYDATA , 0 , (LPARAM)&cpd );
			::SendMessage( hwnd , WM_ACTIVATE , 0 , 0 );
			CloseHandle( mutex );
			return 0;
		}
	}

	SetEditUltraMainConfigDefault( & g_stEditUltraMainConfig );

	// SetStyleThemeDefault( & (g_pstWindowTheme->stStyleTheme) );

	INIT_LIST_HEAD( & listRemoteFileServer );

	InitNavigateBackNextTraceList();

	// TODO: 在此处放置代码。
	LoadConfig();

	// 装载Scintilla控件
	HMODULE hmod = ::LoadLibrary(TEXT("SciLexer.DLL")) ;
	if (hmod == NULL)
	{
		::MessageBox(NULL, TEXT("不能装载Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		CloseHandle( mutex );
		return 1;
	}

	curl_global_init( CURL_GLOBAL_DEFAULT );

	// 执行应用程序初始化:
	MyRegisterClass(hInstance);
	if( ! InitInstance (hInstance, nCmdShow) )
	{
		CloseHandle( mutex );
		return FALSE;
	}

	CloseHandle( mutex );

	UpdateWindowThemeMenu();
	UpdateFileTypeMenu();
	UpdateAllMenus( g_hwndMainWindow , NULL );

#if 0
	{
		char	acInputBuf[ 256 ] ;
		memset( acInputBuf , 0x00 , sizeof(acInputBuf) );
		nret = InputBox( hwndMainClient , "请输入：" , "输入窗口" , 0 , acInputBuf , sizeof(acInputBuf)-1 ) ;
		if( nret == IDOK )
		{
			::MessageBox( NULL , acInputBuf , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else if( nret == IDCANCEL )
		{
			::MessageBox( NULL , "输入窗口被取消" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else
		{
			::MessageBox( NULL , "输入窗口返回错误" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		// exit(0);
	}
#endif

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_EUX));

#if 0
char buf[1024];
memset(buf,0x00,sizeof(buf));
snprintf(buf,sizeof(buf)-1,"argc[%d]",argc);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
for(int i=0;i<argc;i++)
{
snprintf(buf,sizeof(buf)-1,"argv[%d][%s]",i,argv[i]);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
}
exit(0);
#endif
	
	if( g_argc >= 1 )
	{
		for( int i = 0 ; i < g_argc ; i++ )
		{
			if( g_argv[i][strlen(g_argv[i])-1] != '\\' )
				OpenFilesDirectly( g_argv[i] );
			else
				LocateDirectory( g_argv[i] );
		}
	}

	::GetWindowRect( g_hwndMainWindow , & rectMainWindow );
	::PostMessage( g_hwndMainWindow , WM_SIZE , WPARAM(SIZE_MAXSHOW) , (LPARAM)MAKELONG(rectMainWindow.right-rectMainWindow.left,rectMainWindow.bottom-rectMainWindow.top) );

	if( g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval > 0 )
	{
		ReloadSymbolListOrTreeThreadHandler = _beginthread( ReloadSymbolListOrTreeThreadEntry , 0 , NULL ) ;
		if( ReloadSymbolListOrTreeThreadHandler == -1L )
		{
			ErrorBox( "创建定时刷新符号列表或符号树线程失败" );
			return 1;
		}
	}

	MSG msg;

	// 主消息循环:
	while( GetMessage( & msg , nullptr , 0 , 0 ) )
	{
		if ( !IsDialogMessage( hwndSearchFind, &msg ) && !IsDialogMessage( hwndSearchReplace, & msg ) )
		{
			nret = BeforeWndProc( & msg ) ;
			if( nret > 0 )
				continue;

			if( ! TranslateAccelerator( g_hwndMainWindow , hAccelTable , & msg ) )
			{
				TranslateMessage( & msg );

				DispatchMessage( & msg );
			}

			nret = AfterWndProc( & msg ) ;
			if( nret > 0 )
				continue;
		}
	}

	SaveMainConfigFile();
	SaveStyleThemeConfigFile();

	return (int) msg.wParam;
}

//
//  函数: MyRegisterClass()
//
//  目标: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style		= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_EDITULTRA));
	wcex.hCursor		= LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= MAKEINTRESOURCEA(IDC_EUX);
	wcex.lpszClassName	= g_acWindowClassName;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return ::RegisterClassEx(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目标: 保存实例句柄并创建主窗口
//
//   注释:
//
//		在此函数中，我们在全局变量中保存实例句柄并
//		创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HFONT		hControlFont ;
	long		lControlStyle ;
	RECT		rectClient ;

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	BOOL bret = InitCommonControlsEx( & icex ) ;
	if( bret != TRUE )
	{
		::MessageBox(NULL, TEXT("不能注册通用控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	hControlFont = ::CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font) ;

	g_hAppInstance = hInstance; // 将实例句柄存储在全局变量中
   
	g_hwndMainWindow = ::CreateWindowEx( WS_EX_ACCEPTFILES, g_acWindowClassName, g_acAppName, WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_CLIPCHILDREN, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	if (!g_hwndMainWindow)
	{
		return FALSE;
	}

#if 0
	g_hwndReBar = ::CreateWindowEx( WS_EX_TOOLWINDOW , REBARCLASSNAME , "" ,  WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|CCS_NODIVIDER|RBS_VARHEIGHT|RBS_BANDBORDERS , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;

	g_hwndToolBar_FileMenu = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS , 0 , 0 , 0 , 0 , g_hwndReBar , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList_FileMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 4 , 0 ) ;
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_C)) , 0 );
	ImageList_Add( hColorInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_C)) , 0 );
	HIMAGELIST hDisabledInmageList_FileMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 4 , 0 ) ;
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_D)) , 0 );
	ImageList_Add( hDisabledInmageList_FileMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_D)) , 0 );
	TBBUTTON tbToolbar_FileMenu[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 0 , IDM_FILE_NEW , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 1 , IDM_FILE_OPEN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 2 , IDM_FILE_SAVE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 3 , IDM_FILE_SAVEAS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar_FileMenu , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar_FileMenu , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0])) , (LPARAM)&tbToolbar_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList_FileMenu );
	SendMessage( g_hwndToolBar_FileMenu , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar_FileMenu , TB_AUTOSIZE , 0 , 0 );
	DWORD dwBtnSize_FileMenu = (DWORD) SendMessage( g_hwndToolBar_FileMenu , TB_GETBUTTONSIZE , 0 ,0 ) ;
	REBARBANDINFO rbBand_FileMenu = { sizeof(REBARBANDINFO) } ;
	// rbBand_FileMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_FileMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_FileMenu.fStyle = RBBS_CHILDEDGE|RBBS_GRIPPERALWAYS|RBBS_VARIABLEHEIGHT ;
	rbBand_FileMenu.lpText  = (char*)"ToolBar_FileMenu" ;
	rbBand_FileMenu.hwndChild = g_hwndToolBar_FileMenu ;
	// rbBand_FileMenu.cyChild = LOWORD(dwBtnSize_FileMenu);
	rbBand_FileMenu.cxMinChild = sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0]) * HIWORD(dwBtnSize_FileMenu) ;
	rbBand_FileMenu.cyMinChild = LOWORD(dwBtnSize_FileMenu) ;
	rbBand_FileMenu.cx = sizeof(tbToolbar_FileMenu)/sizeof(tbToolbar_FileMenu[0]) * HIWORD(dwBtnSize_FileMenu) ;
	SendMessage( g_hwndReBar , RB_INSERTBAND , (WPARAM)-1 , (LPARAM)&rbBand_FileMenu );

	g_hwndToolBar_EditMenu = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS , 0 , 0 , 0 , 0 , g_hwndReBar , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList_EditMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 3 , 0 ) ;
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_C)) , 0 );
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_C)) , 0 );
	ImageList_Add( hColorInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_C)) , 0 );
	HIMAGELIST hDisabledInmageList_EditMenu = ImageList_Create( 16 , 16 , ILC_COLOR24 /*ILC_COLOR16 | ILC_MASK*/ , 3 , 0 ) ;
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_D)) , 0 );
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_D)) , 0 );
	ImageList_Add( hDisabledInmageList_EditMenu , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_D)) , 0 );
	TBBUTTON tbToolbar_EditMenu[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 1 , IDM_EDIT_CUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 2 , IDM_EDIT_COPY , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 3 , IDM_EDIT_PASTE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , 0 } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar_EditMenu , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar_EditMenu , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0])) , (LPARAM)&tbToolbar_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList_EditMenu );
	SendMessage( g_hwndToolBar_EditMenu , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar_EditMenu , TB_AUTOSIZE , 0 , 0 );
	DWORD dwBtnSize_EditMenu = (DWORD) SendMessage( g_hwndToolBar_EditMenu , TB_GETBUTTONSIZE , 0 ,0 ) ;
	REBARBANDINFO rbBand_EditMenu = { sizeof(REBARBANDINFO) } ;
	// rbBand_EditMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_EditMenu.fMask  = RBBIM_STYLE|RBBIM_TEXT|RBBIM_CHILD|RBBIM_CHILDSIZE|RBBIM_SIZE ;
	rbBand_EditMenu.fStyle = RBBS_CHILDEDGE|RBBS_GRIPPERALWAYS|RBBS_VARIABLEHEIGHT ;
	rbBand_EditMenu.lpText  = (char*)"ToolBar_EditMenu" ;
	rbBand_EditMenu.hwndChild = g_hwndToolBar_EditMenu ;
	// rbBand_EditMenu.cyChild = LOWORD(dwBtnSize_EditMenu);
	rbBand_EditMenu.cxMinChild = sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0]) * HIWORD(dwBtnSize_EditMenu) ;
	rbBand_EditMenu.cyMinChild = LOWORD(dwBtnSize_EditMenu) ;
	rbBand_EditMenu.cx = sizeof(tbToolbar_EditMenu)/sizeof(tbToolbar_EditMenu[0]) * HIWORD(dwBtnSize_EditMenu) ;
	SendMessage( g_hwndReBar , RB_INSERTBAND , (WPARAM)-1 , (LPARAM)&rbBand_EditMenu );

	::GetClientRect( g_hwndReBar , & rectClient );
	g_nReBarHeight = rectClient.bottom - rectClient.top ;
#endif
	g_hwndToolBar = ::CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS|TBSTYLE_FLAT , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;
	HIMAGELIST hColorInmageList = ImageList_Create( 16 , 16 , ILC_COLOR32|ILC_MASK , 26 , 0 ) ;
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEALLFILES_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CLOSEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_UNDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FIND_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDPREV_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDNEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FOUNDLIST_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REPLACE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NAVIGATEBACK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FILETREE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_STYLETHEME_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMOUT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMIN_C)) , RGB(255,255,255) );
	ImageList_AddMasked( hColorInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ABOUT_C)) , RGB(255,255,255) );
	HIMAGELIST hDisabledInmageList = ImageList_Create( 16 , 16 , ILC_COLOR24|ILC_MASK , 26 , 0 ) ;
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NEWFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_OPENFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEFILEAS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_SAVEALLFILES_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CLOSEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_UNDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_CUTTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_COPYTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_PASTETEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FIND_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDPREV_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FINDNEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FOUNDLIST_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_REPLACE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_NAVIGATEBACK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_FILETREE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_STYLETHEME_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMOUT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ZOOMIN_D)) , RGB(255,255,255) );
	ImageList_AddMasked( hDisabledInmageList , LoadBitmap(g_hAppInstance,MAKEINTRESOURCE(IDB_ABOUT_D)) , RGB(255,255,255) );
	TBBUTTON tbToolbar[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 0 , IDM_FILE_NEW , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"新建文件" } ,
		{ 1 , IDM_FILE_OPEN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"打开文件..." } ,
		{ 2 , IDM_FILE_SAVE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存文件" } ,
		{ 3 , IDM_FILE_SAVEAS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"另存为..." } ,
		{ 4 , IDM_FILE_SAVEALL , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存所有文件" } ,
		{ 5 , IDM_FILE_CLOSE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关闭文件" } ,
		{ 6 , IDM_FILE_REMOTE_FILESERVERS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"配置远程文件服务器..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 7 , IDM_EDIT_UNDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"撤销操作" } ,
		{ 8 , IDM_EDIT_REDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"重做操作" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 9 , IDM_EDIT_CUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"剪切文本" } ,
		{ 10 , IDM_EDIT_COPY , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"复制文本" } ,
		{ 11 , IDM_EDIT_PASTE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"粘贴文本" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 12 , IDM_SEARCH_FIND , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找文本..." } ,
		{ 13 , IDM_SEARCH_FINDPREV , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找上一个文本" } ,
		{ 14 , IDM_SEARCH_FINDNEXT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找下一个文本" } ,
		{ 15 , IDM_SEARCH_FOUNDLIST , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"列出包含关键词的行..." } ,
		{ 16 , IDM_SEARCH_REPLACE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"替换文本" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 17 , IDM_SEARCH_TOGGLE_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"切换书签" } ,
		{ 18 , IDM_SEARCH_GOTO_PREV_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到上一个书签" } ,
		{ 19 , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到下一个书签" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 20 , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"退回上一个位置" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 21 , IDM_VIEW_FILETREE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"显示/隐藏文件树" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 22 , IDM_VIEW_MODIFY_STYLETHEME , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"修改主题方案..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 23 , IDM_VIEW_ZOOMOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"放大视图" } ,
		{ 24 , IDM_VIEW_ZOOMIN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"缩小视图" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 25 , IDM_ABOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关于..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( g_hwndToolBar , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( g_hwndToolBar , TB_ADDBUTTONS , (WPARAM)(sizeof(tbToolbar)/sizeof(tbToolbar[0])) , (LPARAM)&tbToolbar );
	SendMessage( g_hwndToolBar , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)hColorInmageList );
	SendMessage( g_hwndToolBar , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)hDisabledInmageList );
	SendMessage( g_hwndToolBar , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( g_hwndToolBar , TB_AUTOSIZE , 0 , 0 );

	::GetClientRect( g_hwndToolBar , & rectClient );
	g_nToolBarHeight = rectClient.bottom - rectClient.top ;

	/*
	::InvalidateRect( g_hwndReBar , NULL , TRUE );
	::UpdateWindow( g_hwndReBar );
	*/

	g_hwndStatusBar = ::CreateWindow( STATUSCLASSNAME , "" , SBS_SIZEGRIP|WS_CHILD|WS_VISIBLE , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , hInstance , NULL ) ;
	::GetClientRect( g_hwndStatusBar , & rectClient );
	g_nStatusBarHeight = rectClient.bottom - rectClient.top ;

	hwndSearchFind = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_FIND_DIALOG), g_hwndMainWindow , SearchFindWndProc ) ;
	if (!hwndSearchFind)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchFind , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IDC_AREATYPE_ALLOPENEDFILES , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WHOLEWORD , false );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_MATCHCASE , true );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WORDSTART , false );
	::CheckRadioButton( hwndSearchFind , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndEditBoxInSearchFind = GetDlgItem( hwndSearchFind , IDC_SEARCH_TEXT ) ;
	ShowWindow( hwndSearchFind , SW_HIDE );

	hwndSearchFound = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_FOUND_DIALOG), g_hwndMainWindow , SearchFoundWndProc ) ;
	if (!hwndSearchFound)
	{
		return FALSE;
	}
	hwndListBoxInSearchFound = GetDlgItem( hwndSearchFound , IDC_FOUNDLIST ) ;
	::SendMessage( hwndListBoxInSearchFound , WM_SETFONT , (WPARAM)hControlFont, 0);
	lControlStyle = ::GetWindowLong( hwndListBoxInSearchFound , GWL_STYLE ) ;
	lControlStyle |= LBS_NOTIFY|LBS_USETABSTOPS|LBS_HASSTRINGS ;
	::SetWindowLong( hwndListBoxInSearchFound , GWL_STYLE , lControlStyle );
	ShowWindow( hwndSearchFound , SW_HIDE );

	hwndSearchReplace = ::CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_REPLACE_DIALOG), g_hwndMainWindow , SearchReplaceWndProc ) ;
	if (!hwndSearchReplace)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchReplace , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchReplace , IDC_AREATYPE_THISFILE , IDC_AREATYPE_ALLOPENEDFILES , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_WHOLEWORD , false );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_MATCHCASE , true );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_WORDSTART , false );
	::CheckRadioButton( hwndSearchReplace , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndFromEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_FROMTEXT ) ;
	hwndToEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_TOTEXT ) ;
	ShowWindow( hwndSearchReplace , SW_HIDE );

	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );

	UpdateAllWindows( g_hwndMainWindow );

	ShowWindow(g_hwndMainWindow, SW_SHOWMAXIMIZED);
	UpdateWindow(g_hwndMainWindow);

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目标: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//

static int OnCreateWindow( HWND hWnd , WPARAM wParam , LPARAM lParam )
{
	BOOL	bret ;
	int	nret ;
	LSTATUS	lsret ;
	HKEY	regkey_EditUltra ;

	//// ::EnableMenuItem(::GetSubMenu(::GetMenu(hwndMainClient),3), 0, MF_DISABLED | MF_GRAYED | MF_BYCOMMAND);

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_TAB_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		::MessageBox(NULL, TEXT("不能注册TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nret = CreateFileTreeBar( hWnd ) ;
	if( nret )
		return nret;

	nret = CreateTabPages( hWnd ) ;
	if( nret )
		return nret;

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, true);

		g_bIsEnvFilePopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, false);

		g_bIsEnvFilePopupMenuSelected = FALSE ;
	}

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, true);

		g_bIsEnvDirectoryPopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, false);

		g_bIsEnvDirectoryPopupMenuSelected = FALSE ;
	}

	return 0;
}

void UpdateStatusBarPathFilenameInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
		return ;

	snprintf( buf , sizeof(buf)-1 , "路径文件名:%s" , g_pnodeCurrentTabPage->acPathFilename );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_PATHFILENAME , (LPARAM)buf );

	return;
}

void UpdateStatusBarLocationInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
		return ;

	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	int nCurrentLineNo = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;
	int nFirstPosInCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_POSITIONFROMLINE , nCurrentLineNo , 0 ) ;
	int nLastPosInCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETLINEENDPOSITION , nCurrentLineNo , 0 ) ;
	int nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
	int nMaxLineNo = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "列:%d/%d    行:%d/%d    偏移量:%d/%d"
		, nCurrentPos-nFirstPosInCurrentLine+1 , nLastPosInCurrentLine-nFirstPosInCurrentLine+1
		, nCurrentLineNo+1 , nMaxLineNo
		, nCurrentPos , nMaxPos );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_LOCATIONINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarEolModeInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
		return ;

	memset( buf , 0x00 , sizeof(buf) );
	int nEolMode = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETEOLMODE , 0 , 0 ) ;
	if( nEolMode == 0 )
		strcpy( buf , "换行符模式:DOS" );
	else if( nEolMode == 1 )
		strcpy( buf , "换行符模式:MAC" );
	else if( nEolMode == 2 )
		strcpy( buf , "换行符模式:UNIX/Linux" );
	else
		strcpy( buf , "未知换行符模式" );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_EOLMODEINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarEncodingInfo()
{
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
		return ;

	memset( buf , 0x00 , sizeof(buf) );
	if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
		strcpy( buf , "字符编码:UTF8" );
	else if( g_pnodeCurrentTabPage->nCodePage == ENCODING_GBK )
		strcpy( buf , "字符编码:GBK" );
	else if( g_pnodeCurrentTabPage->nCodePage == ENCODING_BIG5 )
		strcpy( buf , "字符编码:BIG5" );
	else
		strcpy( buf , "未知字符编码" );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_ENCODINGINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBarSelectionInfo()
{
	int	nSelectionStart ;
	int	nSelectionEnd ;
	char	buf[ 1024 ] ;

	if( g_pnodeCurrentTabPage == NULL )
		return ;

	nSelectionStart = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
	nSelectionEnd = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;

	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "选择文本长度:%d" , nSelectionEnd-nSelectionStart );
	::SendMessage( g_hwndStatusBar , SB_SETTEXT , (WPARAM)STATUSBAR_ITEM_INDEX_SELECTIONINFO , (LPARAM)buf );

	return;
}

void UpdateStatusBar( HWND hWnd )
{
	RECT	rectClient ;
	int	anStatusBarPartRightEdge[ STATUSBAR_ITEM_COUNT ] ;

	::GetClientRect( hWnd , & rectClient );

	::SetWindowPos ( g_hwndStatusBar , HWND_TOP ,  rectClient.left , rectClient.bottom-g_nStatusBarHeight , rectClient.right-rectClient.left , rectClient.bottom , SWP_SHOWWINDOW );

	anStatusBarPartRightEdge[4] = rectClient.right ;
	anStatusBarPartRightEdge[3] = anStatusBarPartRightEdge[4] - 150 ;
	anStatusBarPartRightEdge[2] = anStatusBarPartRightEdge[3] - 150 ;
	anStatusBarPartRightEdge[1] = anStatusBarPartRightEdge[2] - 150 ;
	anStatusBarPartRightEdge[0] = anStatusBarPartRightEdge[1] - 300 ;
	::SendMessage( g_hwndStatusBar , SB_SETPARTS , STATUSBAR_ITEM_COUNT , (LPARAM)(LPINT)anStatusBarPartRightEdge );

	UpdateStatusBarPathFilenameInfo();
	UpdateStatusBarLocationInfo();
	UpdateStatusBarEolModeInfo();
	UpdateStatusBarEncodingInfo();
	UpdateStatusBarSelectionInfo();

	// ::ShowWindow( g_hwndStatusBar , SW_SHOW );
	::InvalidateRect( g_hwndStatusBar , NULL , TRUE );
	::UpdateWindow( g_hwndStatusBar );

	return;
}

void UpdateAllWindows( HWND hWnd )
{
	RECT	rectAdjust ;
	RECT	rectFileTree ;

	CalcTabPagesHeight();

	AdjustTabPages();

	::SetWindowPos( g_hwndTabPages , HWND_TOP , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , SWP_SHOWWINDOW );
	memcpy( & rectAdjust , & g_rectTabPages , sizeof(RECT) );
	TabCtrl_AdjustRect( g_hwndTabPages , FALSE , & rectAdjust );
	
	CalcTabPagesHeight();

	AdjustTabPages();

	if( g_bIsFileTreeBarShow == TRUE )
	{
		::SetWindowPos ( g_hwndFileTreeBar , HWND_TOP ,  g_rectFileTreeBar.left , g_rectFileTreeBar.top , g_rectFileTreeBar.right-g_rectFileTreeBar.left , g_rectFileTreeBar.bottom-g_rectFileTreeBar.top , SWP_SHOWWINDOW );
		memcpy( & rectAdjust , & g_rectFileTreeBar , sizeof(RECT) );
		TabCtrl_AdjustRect( g_hwndFileTreeBar , FALSE , & rectAdjust );
		// ::ShowWindow( g_hwndFileTreeBar , (g_bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
		::ShowWindow( g_hwndFileTreeBar , SW_SHOW );
		::UpdateWindow( g_hwndFileTreeBar );
	}
	else
	{
		::SetWindowPos ( g_hwndFileTreeBar , 0 ,  0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		::ShowWindow( g_hwndFileTreeBar , SW_HIDE );
		::UpdateWindow( g_hwndFileTreeBar );
	}
	
	AdjustFileTreeBox( & g_rectFileTreeBar , & rectFileTree );

	if( g_bIsFileTreeBarShow == TRUE )
	{
		::SetWindowPos( g_hwndFileTree , HWND_TOP , rectFileTree.left , rectFileTree.top , rectFileTree.right-rectFileTree.left , rectFileTree.bottom-rectFileTree.top , SWP_SHOWWINDOW );
		// ::ShowWindow( g_hwndFileTree , (g_bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
		::ShowWindow( g_hwndFileTree , SW_SHOW );
		::InvalidateRect( g_hwndFileTree , NULL , TRUE );
		::UpdateWindow( g_hwndFileTree );
	}
	else
	{
		::SetWindowPos( g_hwndFileTree , 0 , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		::ShowWindow( g_hwndFileTree , SW_HIDE );
		::UpdateWindow( g_hwndFileTree );
	}

	::SetWindowPos( g_hwndTabPages , HWND_TOP , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , SWP_SHOWWINDOW );
	memcpy( & rectAdjust , & g_rectTabPages , sizeof(RECT) );
	TabCtrl_AdjustRect( g_hwndTabPages , FALSE , & rectAdjust );
	/*
	::ShowWindow( g_hwndTabPages , SW_SHOW);
	::InvalidateRect( g_hwndTabPages , NULL , TRUE );
	::UpdateWindow( g_hwndTabPages );
	*/
	
	int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( int nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		TCITEM tci ;
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		struct TabPage *pnodeTabPage = (struct TabPage *)(tci.lParam);
		if( pnodeTabPage != g_pnodeCurrentTabPage )
		{
			::SetWindowPos( pnodeTabPage->hwndScintilla , 0 , pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.top , pnodeTabPage->rectScintilla.right-pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.bottom-pnodeTabPage->rectScintilla.top , SWP_HIDEWINDOW );

			if( pnodeTabPage->hwndSymbolList )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolList , 0 , pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.top , pnodeTabPage->rectSymbolList.right-pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.bottom-pnodeTabPage->rectSymbolList.top , SWP_HIDEWINDOW );
				/*
				::ShowWindow( pnodeTabPage->hwndSymbolList , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolList , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolList );
				*/
			}

			if( pnodeTabPage->hwndSymbolTree )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolTree , 0 , pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.top , pnodeTabPage->rectSymbolTree.right-pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.bottom-pnodeTabPage->rectSymbolTree.top , SWP_HIDEWINDOW );
				/*
				::ShowWindow( pnodeTabPage->hwndSymbolTree , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolTree , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolTree );
				*/
			}

			if( pnodeTabPage->hwndQueryResultEdit )
			{
				/*
				::ShowWindow( pnodeTabPage->hwndQueryResultEdit , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultEdit );
				*/
			}

			if( pnodeTabPage->hwndQueryResultTable )
			{
				/*
				::ShowWindow( pnodeTabPage->hwndQueryResultTable , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultTable );
				*/
			}
		}
	}
	
	if( g_pnodeCurrentTabPage )
	{
		AdjustTabPageBox( g_pnodeCurrentTabPage );

		::SetWindowPos( g_pnodeCurrentTabPage->hwndScintilla , HWND_TOP , g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.top , g_pnodeCurrentTabPage->rectScintilla.right-g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.bottom-g_pnodeCurrentTabPage->rectScintilla.top , SWP_SHOWWINDOW );
		::InvalidateRect( g_pnodeCurrentTabPage->hwndScintilla , NULL , TRUE );
		::UpdateWindow( g_pnodeCurrentTabPage->hwndScintilla );

		if( g_pnodeCurrentTabPage->hwndSymbolList )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolList , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.top , g_pnodeCurrentTabPage->rectSymbolList.right-g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.bottom-g_pnodeCurrentTabPage->rectSymbolList.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolList , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolList , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolList );
		}

		if( g_pnodeCurrentTabPage->hwndSymbolTree )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolTree , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.top , g_pnodeCurrentTabPage->rectSymbolTree.right-g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.bottom-g_pnodeCurrentTabPage->rectSymbolTree.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolTree , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolTree , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolTree );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultEdit )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultEdit , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->rectQueryResultEdit.right-g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.bottom-g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->hwndQueryResultEdit?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultEdit , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultTable )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultTable , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->rectQueryResultListView.right-g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.bottom-g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->hwndQueryResultTable?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultTable , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultTable , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultTable );
		}

		::SetFocus( g_pnodeCurrentTabPage->hwndScintilla );
	}

	::UpdateWindow( g_hwndToolBar );
	// ::InvalidateRect( g_hwndReBar , NULL , TRUE );
	// ::UpdateWindow( g_hwndReBar );

	UpdateStatusBar( hWnd );

	// ::InvalidateRect( g_hwndMainWindow , NULL , TRUE );
	// ::UpdateWindow( g_hwndMainWindow );

	return;
}

void OnResizeWindow( HWND hWnd , int nWidth , int nHeight )
{
	if( g_hwndFileTreeBar == NULL || g_hwndTabPages == NULL )
		return;

	g_rectTabPages.right = g_rectTabPages.left + nWidth ;
	g_rectTabPages.bottom = g_rectTabPages.top + nHeight ;

	UpdateAllWindows( hWnd );

	return;
}

void PushOpenPathFilenameRecently( char *acPathFilename )
{
	int	index ;
	int	start ;
	int	end ;

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( strcmp( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] , acPathFilename ) == 0 )
			break;
	}
	if( index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT )
	{
		start = 0 ;
		end = index ;
	}
	else
	{
		start = 0 ;
		end = OPEN_PATHFILENAME_RECENTLY_MAXCOUNT - 1 ;
	}

	for( index = end - 1 ; index >= start ; index-- )
	{
		strcpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index+1] , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}
	strncpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0] , acPathFilename , sizeof(g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0])-1 );

	return;
}

void UpdateOpenPathFilenameRecently()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_FILE ;
	HMENU		hMenu_OPENRECENTLYFILES ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = ::GetMenu(g_hwndMainWindow) ;
	hMenu_FILE = ::GetSubMenu( hRootMenu , 0 ) ;
	hMenu_OPENRECENTLYFILES = ::GetSubMenu( hMenu_FILE , 2 ) ;
	count = ::GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = ::DeleteMenu( hMenu_OPENRECENTLYFILES , 0 , MF_BYPOSITION );
	}

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index][0] == '\0' )
			break;

		bret = ::AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE+index , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}

	count = ::GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	if( count == 0 )
	{
		bret = ::AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , "（空）" );
		SetMenuItemEnable( g_hwndMainWindow , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , FALSE );
	}

	return;
}

void SetCurrentFileTypeChecked( HWND hWnd , struct TabPage *pnodeTabPage )
{
	struct DocTypeConfig	*pstDocTypeConfig = NULL ;
	int			index ;

	for( index = 0 , pstDocTypeConfig = g_astDocTypeConfig ; pstDocTypeConfig->nDocType != DOCTYPE_END ; index++ , pstDocTypeConfig++ )
	{
		SetMenuItemChecked( hWnd , IDM_VIEW_SWITCH_FILETYPE_BASE+index , (pnodeTabPage&&(pnodeTabPage->pstDocTypeConfig==pstDocTypeConfig)) );
	}

	return;
}

void SetCurrentWindowThemeChecked( HWND hWnd )
{
	int	index ;

	for( index = 0 ; index < g_nWindowThemeCount ; index++ )
	{
		SetMenuItemChecked( hWnd , IDM_VIEW_SWITCH_STYLETHEME_BASE+index , g_pstWindowTheme==&(g_astWindowTheme[index]) );
	}

	return;
}

void SetViewTabWidthMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , g_stEditUltraMainConfig.nTabWidth , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*p = NULL ;
	char		acMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	p = strchr( acMenuText , '[' ) ;
	if( p )
	{
		*(p+1) = (g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters%10) + '0' ;
	}
	mii.cch = (int)strlen(acMenuText) - 1 ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void SetReloadSymbolListOrTreeIntervalMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void UpdateAllMenus( HWND hWnd , struct TabPage *pnodeTabPage )
{
	SetMenuItemChecked( hWnd , IDM_FILE_SETREADONLY_AFTER_OPEN , g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile );
	UpdateOpenPathFilenameRecently();
	SetMenuItemChecked( hWnd , IDM_FILE_UPDATE_WHERE_SELECTTABPAGE , g_stEditUltraMainConfig.bUpdateWhereSelectTabPage );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_WINDOWS_EOLS , (g_stEditUltraMainConfig.nNewFileEols==0) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_MAC_EOLS , (g_stEditUltraMainConfig.nNewFileEols==1) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_UNIX_EOLS , (g_stEditUltraMainConfig.nNewFileEols==2) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_UTF8 , (g_stEditUltraMainConfig.nNewFileEncoding==65001) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_GB18030 , (g_stEditUltraMainConfig.nNewFileEncoding==936) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_BIG5 , (g_stEditUltraMainConfig.nNewFileEncoding==950) );

	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR , g_stEditUltraMainConfig.bEnableAutoAddCloseChar );
	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_INDENTATION , g_stEditUltraMainConfig.bEnableAutoIdentation );

	SetCurrentFileTypeChecked( hWnd , pnodeTabPage );
	SetCurrentWindowThemeChecked( hWnd );
	SetMenuItemChecked( hWnd , IDM_VIEW_FILETREE , g_bIsFileTreeBarShow );
	SetViewTabWidthMenuText( IDM_VIEW_TAB_WIDTH );
	SetMenuItemChecked( hWnd , IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES , g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces );
	SetMenuItemChecked( hWnd , IDM_VIEW_WRAPLINE_MODE , g_stEditUltraMainConfig.bWrapLineMode );
	SetMenuItemChecked( hWnd , IDM_VIEW_LINENUMBER_VISIABLE , g_stEditUltraMainConfig.bLineNumberVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_BOOKMARK_VISIABLE , g_stEditUltraMainConfig.bBookmarkVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_WHITESPACE_VISIABLE , g_stEditUltraMainConfig.bWhiteSpaceVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_NEWLINE_VISIABLE , g_stEditUltraMainConfig.bNewLineVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_INDENTATIONGUIDES_VISIABLE , g_stEditUltraMainConfig.bIndentationGuidesVisiable );

	SetReloadSymbolListOrTreeIntervalMenuText( IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL );
	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW , g_stEditUltraMainConfig.bEnableAutoCompletedShow );
	SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS );
	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_CALLTIPSHOW , g_stEditUltraMainConfig.bEnableCallTipShow );

	SetMenuItemChecked( hWnd , IDM_SOURCECODE_BLOCKFOLD_VISIABLE , g_stEditUltraMainConfig.bBlockFoldVisiable );
	
	if( pnodeTabPage )
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , pnodeTabPage && IsDocumentModified(pnodeTabPage) );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , pnodeTabPage && pnodeTabPage->acFilename[0] );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==0) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==1) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==2) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_UTF8 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==65001) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_GB18030 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==936) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_BIG5 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==950) );
	}
	else
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , FALSE );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , FALSE );

		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , FALSE );
	}

	if( pnodeTabPage == NULL )
	{
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVE , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_CLOSE , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_UNDO , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_REDO , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_CUT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_COPY , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_PASTE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FIND , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , FALSE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , FALSE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}
	else
	{
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVE , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_CLOSE , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_UNDO , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_REDO , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_CUT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_COPY , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_EDIT_PASTE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FIND , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , TRUE );
		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , TRUE );

		::SendMessage( g_hwndToolBar , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}

	return;
}

int BeforeWndProc( MSG *p_msg )
{
	NMHDR		*lpnmhdr = NULL;

	int		nret = 0 ;

	if( p_msg->message == WM_SYSKEYDOWN && 49 <= p_msg->wParam && p_msg->wParam <= 57 && (p_msg->lParam&(1<<29)) )
	{
		struct TabPage	*pnodeTabPage = SelectTabPageByIndex( (unsigned int)(p_msg->wParam) - 49 ) ;
		if( pnodeTabPage )
			return 1;
	}

	if( p_msg->message == WM_MOUSEMOVE )
	{
		WPARAM wParam ;
		LPARAM lParam ;
		POINT pt ;
		wParam = p_msg->wParam ;
		lParam = p_msg->lParam ;
		pt.x = LOWORD(lParam) ;
		pt.y = HIWORD(lParam) ;
		if( p_msg->hwnd != g_hwndMainWindow )
		{
			::ClientToScreen( p_msg->hwnd , & pt );
			::ScreenToClient( g_hwndMainWindow , & pt );
			lParam = MAKELONG( pt.x , pt.y ) ;
		}
		wParam = MAKELONG( IDM_MOUSEMOVE , 0 ) ;
		::PostMessage( g_hwndMainWindow , WM_COMMAND , wParam , lParam);
	}
	
	if( p_msg->message == WM_LBUTTONUP )
	{
		if( p_msg->hwnd != g_hwndMainWindow )
		{
			::PostMessage( g_hwndMainWindow , WM_LBUTTONUP , p_msg->wParam , p_msg->lParam);
		}
	}

	if( p_msg->hwnd == g_hwndFileTree )
	{
		if( p_msg->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			TVHITTESTINFO tvhti ;
			memset( & tvhti , 0x00 , sizeof(tvhti) );
			memcpy( & (tvhti.pt) , & pt , sizeof(POINT) );
			TreeView_HitTest( g_hwndFileTree , & tvhti );
			struct TreeViewData *tvd = GetTreeViewDataFromHTREEITEM( tvhti.hItem ) ;
			if( tvd )
			{
				::ClientToScreen( p_msg->hwnd , & pt );
				if( tvd->nImageIndex == nFileTreeImageDrive || tvd->nImageIndex == nFileTreeImageOpenFold || tvd->nImageIndex == nFileTreeImageClosedFold )
					::TrackPopupMenu( g_hFileTreeDirectoryPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
				else
					::TrackPopupMenu( g_hFileTreeFilePopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			}
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_REFRESH_FILETREE )
		{
			RefreshFileTree();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_CREATE_SUB_DIRECTORY )
		{
			FileTreeCreateSubDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RENAME_DIRECTORY )
		{
			FileTreeRenameDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_DELETE_DIRECTORY )
		{
			FileTreeDeleteDirectory();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_CREATE_FILE )
		{
			FileTreeCreateFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_COPY_FILE )
		{
			FileTreeCopyFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RENAME_FILE )
		{
			FileTreeRenameFile();
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_DELETE_FILE )
		{
			FileTreeDeleteFile();
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_LBUTTONDOWN )
		{
			if( g_bIsTabPageMoving == FALSE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				g_nTabPageMoveFrom = -1 ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						g_nTabPageMoveFrom = nTabPageIndex ;
						break;
					}
				}
				if( g_nTabPageMoveFrom >= 0 )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					g_bIsTabPageMoving = TRUE ;
					g_rectLMouseDown.x = x ;
					g_rectLMouseDown.y = y ;
				}
			}
		}
		else if( p_msg->message == WM_LBUTTONUP )
		{
			if( g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				if( abs(x-g_rectLMouseDown.x) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK || abs(y-g_rectLMouseDown.y) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK )
				{
					int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
					RECT rectTabPage ;
					int nTabPageMoveTo = -1 ;
					for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
					{
						TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
						if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
						{
							nTabPageMoveTo = nTabPageIndex ;
							break;
						}
					}
					if( nTabPageMoveTo >= 0 )
					{
						TabCtrl_DeleteItem( g_hwndTabPages , g_nTabPageMoveFrom );

						TCITEM		tci ;

						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_TEXT|TCIF_PARAM ;
						tci.pszText = g_pnodeCurrentTabPage->acFilename ;
						tci.lParam = (LPARAM)g_pnodeCurrentTabPage ;
						nret = TabCtrl_InsertItem( g_hwndTabPages , nTabPageMoveTo , & tci ) ;
						if( nret == -1 )
						{
							::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
						}
						else
						{
							SelectTabPageByIndex( nTabPageMoveTo );
						}
					}
				}

				::SetCursor( LoadCursor(NULL,IDC_ARROW) );
				g_bIsTabPageMoving = FALSE ;
			}
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_KEYDOWN )
		{
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , p_msg->wParam , (::GetKeyState(VK_CONTROL)&0x8000)?VK_CONTROL:0 );
		}
		else if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hEditorPopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolList )
	{
		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hSymbolListPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_SYMBOLLIST )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolTree )
	{
		if( p_msg->message == WM_LBUTTONDBLCLK )
		{
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
		else if( p_msg->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hSymbolTreePopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_SYMBOLTREE )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	return 0;
}

int AfterWndProc( MSG *p_msg )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	struct TabPage	*p = NULL ;
	TCITEM		tci ;

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_RBUTTONDOWN )
		{
			int x = LOWORD(p_msg->lParam) ;
			int y = HIWORD(p_msg->lParam) ;
			int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
			RECT rectTabPage , rectFileTreeBar ;
			for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
			{
				int	x1 , x2 ;

				TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
				x1 = rectTabPage.left ;
				x2 = rectTabPage.right;
				if( g_bIsFileTreeBarShow == TRUE )
				{
					::GetClientRect( g_hwndFileTreeBar , & rectFileTreeBar );
					x1 += rectFileTreeBar.right + SPLIT_WIDTH ;
					x2 += rectFileTreeBar.right + SPLIT_WIDTH ;
				}
				if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom )
				{
					SelectTabPageByIndex( nTabPageIndex );
					break;
				}
			}
		}
		else if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hTabPagePopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_KEYUP )
		{
			int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			UpdateNavigateBackNextTrace( g_pnodeCurrentTabPage , nCurrentPos );

			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp( g_pnodeCurrentTabPage , p_msg->wParam , p_msg->lParam );

			UpdateStatusBarLocationInfo();
		}
	}
	
	if( p_msg->message == WM_LBUTTONUP )
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);
			if( p_msg->hwnd == p->hwndScintilla )
			{
				int nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				AddNavigateBackNextTrace( p , nCurrentPos );

				if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp )
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp( g_pnodeCurrentTabPage , p_msg->wParam , p_msg->lParam );

				UpdateStatusBarLocationInfo();
			}
		}
	}

	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nWidth , nHeight ;
	int		wmId , wmNC ;
	HWND		wmWnd ;
	NMHDR		*lpnmhdr = NULL;
	SCNotification	*lpnotify = NULL ;
	NMTREEVIEW	*lpnmtv = NULL ;
	TOOLTIPTEXT	*pTTT = NULL ;
	struct TabPage	*pnodeTabPage = NULL ;
	PAINTSTRUCT	ps ;
	HDC		hdc ;

	int		nret = 0;

	if( message == WM_COMMAND )
	{
		wmId = LOWORD(wParam) ;
		if( IDM_FILE_OPEN_RECENTLY_HISTORY_BASE <= wmId && wmId <= IDM_FILE_OPEN_RECENTLY_HISTORY_BASE + OPEN_PATHFILENAME_RECENTLY_MAXCOUNT-1 )
		{
			if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE][0] )
			{
				OpenFileDirectly( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE] );
			}
		}
		if( IDM_VIEW_SWITCH_STYLETHEME_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_STYLETHEME_BASE + VIEW_STYLETHEME_MAXCOUNT-1 )
		{
			if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE][0] )
			{
				OnViewSwitchStyleTheme( wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE );
			}
		}
		if( IDM_VIEW_SWITCH_FILETYPE_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_FILETYPE_BASE + VIEW_FILETYPE_MAXCOUNT-1 )
		{
			OnViewSwitchFileType( wmId-IDM_VIEW_SWITCH_FILETYPE_BASE );
		}
	}

	switch (message)
	{
	case WM_CREATE:
		nret = OnCreateWindow( hWnd , wParam , lParam ) ;
		if( nret )
			PostQuitMessage(0);
		break;
	case WM_SIZE:
		nWidth = LOWORD( lParam );
		nHeight = HIWORD( lParam );
		OnResizeWindow( hWnd , nWidth , nHeight );
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam) ;
		wmNC = HIWORD(wParam) ;
		wmWnd = (HWND)lParam ;

		// 分析菜单选择:
		if( wmNC == LBN_DBLCLK )
		{
			nWidth = 0 ;
			nHeight = 0 ;
		}

		if( g_pnodeCurrentTabPage && wmWnd == g_pnodeCurrentTabPage->hwndSymbolList && wmNC == LBN_DBLCLK )
		{
			return OnSymbolListDbClick( g_pnodeCurrentTabPage );
		}
		else if( wmNC == BN_CLICKED && wmWnd == g_hwndTabCloseButton )
		{
			if( g_pnodeCloseButtonTabPage )
			{
				return OnCloseFile( g_pnodeCloseButtonTabPage );
			}
		}

		switch (wmId)
		{
		case IDM_FILE_NEW:
			OnNewFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_OPEN:
			OnOpenFile();
			break;
		case IDM_FILE_CLEAN_OPEN_RECENTLY_HISTORY:
			OnCleanOpenRecentlyHistory( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SAVE:
			OnSaveFile( g_pnodeCurrentTabPage , FALSE );
			break;
		case IDM_FILE_SAVEAS:
			OnSaveFileAs( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SAVEALL:
			OnSaveAllFiles();
			break;
		case IDM_FILE_CLOSE:
			OnCloseFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_CLOSEALL:
			OnCloseAllFile();
			break;
		case IDM_FILE_CLOSEALL_EXPECT_CURRENT_FILE:
			OnCloseAllExpectCurrentFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_REMOTE_FILESERVERS:
			OnManageRemoteFileServers();
			break;
		case IDM_FILE_SETREADONLY_AFTER_OPEN:
			OnSetReadOnlyAfterOpenFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_UPDATE_WHERE_SELECTTABPAGE:
			OnFileUpdateWhereSelectTabPage( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_NEWFILE_WINDOWS_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_NEWFILE_MAC_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_NEWFILE_UNIX_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_CONVERT_WINDOWS_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_CONVERT_MAC_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_CONVERT_UNIX_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_UTF8:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_GB18030:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_NEWFILE_ENCODING_BIG5:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_FILE_CONVERT_ENCODING_UTF8:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_CONVERT_ENCODING_GB18030:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_CONVERT_ENCODING_BIG5:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_EXIT:
			nret = CheckAllFilesSave() ;
			if( nret )
				break;
			DestroyWindow(hWnd);
			break;
		case IDM_EDIT_UNDO:
			OnUndoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_REDO:
			OnRedoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUT:
			OnCutEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY:
			OnCopyEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTE:
			OnPasteEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETE:
			OnDeleteEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE:
			OnCutLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE_AND_PASTELINE:
			OnCutLineAndPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE:
			OnCopyLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE_AND_PASTELINE:
			OnCopyLineAndPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE:
			OnPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE_UPSTAIRS:
			OnPasteLineUpstairsEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETELINE:
			OnDeleteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_JOINLINE:
			OnJoinLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_LOWERCASE:
			OnLowerCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_UPPERCASE:
			OnUpperCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR:
			OnEditEnableAutoAddCloseChar( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_INDENTATION:
			OnEditEnableAutoIdentation( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_ENCODING:
			OnEditBase64Encoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_DECODING:
			OnEditBase64Decoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_MD5:
			OnEditMd5( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA1:
			OnEditSha1( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA256:
			OnEditSha256( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_ENCRYPTO:
			OnEdit3DesCbcEncrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_DECRYPTO:
			OnEdit3DesCbcDecrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FIND:
			OnSearchFind( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDPREV:
			OnSearchFindPrev( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDNEXT:
			OnSearchFindNext( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FOUNDLIST:
			OnSearchFoundList( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REPLACE:
			OnSearchReplace( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTALL:
			OnSelectAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTWORD:
			OnSelectWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTLINE:
			OnSelectLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_CHARGROUP:
			OnAddSelectLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_CHARGROUP:
			OnAddSelectRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_WORD:
			OnAddSelectLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_WORD:
			OnAddSelectRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_TOP_BLOCKFIRSTLINE:
			OnAddSelectTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_BOTTOM_BLOCKFIRSTLINE:
			OnAddSelectBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_CHARGROUP:
			OnMoveLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_CHARGROUP:
			OnMoveRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_WORD:
			OnMoveLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_WORD:
			OnMoveRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_TOP_BLOCKFIRSTLINE:
			OnMoveTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_BOTTOM_BLOCKFIRSTLINE:
			OnMoveBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_TOGGLE_BOOKMARK:
			OnSearchToggleBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADD_BOOKMARK:
			OnSearchAddBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_BOOKMARK:
			OnSearchRemoveBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_ALL_BOOKMARKS:
			OnSearchRemoveAllBookmarks( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK:
			OnSearchGotoPrevBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK:
			OnSearchGotoNextBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoPrevBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoNextBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTOLINE:
			OnSearchGotoLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE:
			OnSearchNavigateBackPrev_InThisFile();
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_ALL_FILES:
			OnSearchNavigateBackPrev_InAllFiles();
			break;
		case IDM_SEARCH_MULTISELECT_README:
			::MessageBox( NULL , "使用快捷键Ctrl+MouseLButtonClick多位置定位、Ctrl+MouseLButtonDown+MouseMove+MouseLButtonUp多个文本块选择，支持多位置联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_SEARCH_COLUMNSELECT_README:
			::MessageBox( NULL , "使用快捷键Alt+MouseLButtonDown+MouseMove或Alt+Shift+Left/Right/Up/Down进行列选择，支持多行联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_VIEW_FILETREE:
			OnViewFileTree( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_MODIFY_STYLETHEME:
			OnViewModifyThemeStyle();
			break;
		case IDM_VIEW_COPYNEW_STYLETHEME:
			OnViewCopyNewThemeStyle();
			break;
		case IDM_VIEW_WRAPLINE_MODE:
			OnViewWrapLineMode( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_TAB_WIDTH:
			OnViewTabWidth( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES:
			OnViewOnKeydownTabConvertSpaces( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_LINENUMBER_VISIABLE:
			OnViewLineNumberVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_BOOKMARK_VISIABLE:
			OnViewBookmarkVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_WHITESPACE_VISIABLE:
			OnViewWhiteSpaceVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_NEWLINE_VISIABLE:
			OnViewNewLineVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_INDENTATIONGUIDES_VISIABLE:
			OnViewIndentationGuidesVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMOUT:
			OnViewZoomOut( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMIN:
			OnViewZoomIn( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMRESET:
			OnViewZoomReset( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_VISIABLE:
			OnSourceCodeBlockFoldVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_TOGGLE:
			OnSourceCodeBlockFoldToggle( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACT:
			OnSourceCodeBlockFoldContract( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPAND:
			OnSourceCodeBlockFoldExpand( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACTALL:
			OnSourceCodeBlockFoldContractAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPANDALL:
			OnSourceCodeBlockFoldExpandAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_GOTODEF:
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F11 , lParam );
			break;
		case IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL:
			OnSetReloadSymbolListOrTreeInterval( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW:
			OnSourceCodeEnableAutoCompletedShow( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS:
			OnSourceCodeAutoCompletedShowAfterInputCharacters( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_ENABLE_CALLTIPSHOW:
			OnSourceCodeEnableCallTipShow( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_INSERT_DATABASE_CONNECTION_CONFIG:
			OnInsertDataBaseConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_EXECUTE_SQL:
		case IDM_DATABASE_EXECUTE_REDIS_COMMAND:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F5 , 0 );
			break;
		case IDM_DATABASE_AUTO_SELECT_CURRENT_SQL_AND_EXECUTE:
		case IDM_DATABASE_AUTO_SELECT_CURRENT_REDIS_COMMAND_AND_EXECUTE:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F5 , VK_CONTROL );
			break;
		case IDM_DATABASE_INSERT_REDIS_CONNECTION_CONFIG:
			OnInsertRedisConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_FILE_POPUPMENU:
			OnEnvFilePopupMenu();
			break;
		case IDM_ENV_DIRECTORY_POPUPMENU:
			OnEnvDirectoryPopupMenu();
			break;
		case IDM_ENV_SET_PROCESSFILE_CMD:
			OnEnvSetProcessFileCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSFILE_CMD:
			OnEnvExecuteProcessFileCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_SET_PROCESSTEXT_CMD:
			OnEnvSetProcessTextCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSTEXT_CMD:
			OnEnvExecuteProcessTextCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_ABOUT:
			DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_MOUSEMOVE:
			if( g_bIsFileTreeBarShow == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_rectFileTreeBar.top <= y && y <= g_rectFileTreeBar.bottom && g_rectFileTreeBar.right < x && x < g_rectTabPages.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					if( g_bIsFileTreeBarHoverResizing == FALSE )
						g_bIsFileTreeBarHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFileTreeBarHoverResizing == TRUE )
						g_bIsFileTreeBarHoverResizing = FALSE ;
				}

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					g_stEditUltraMainConfig.nFileTreeBarWidth = x - g_rectFileTreeBar.left - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nFileTreeBarWidth < FILETREEBAR_WIDTH_MIN )
						g_stEditUltraMainConfig.nFileTreeBarWidth = FILETREEBAR_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					}
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolList )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFunctionListResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolList.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolList.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolList.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsFunctionListHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFunctionListHoverResizing == TRUE )
						g_bIsFunctionListHoverResizing = FALSE ;
				}

				if( g_bIsFunctionListResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolListWidth = g_pnodeCurrentTabPage->rectSymbolList.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolListWidth < SYMBOLLIST_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolListWidth = SYMBOLLIST_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsTreeViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolTree.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolTree.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolTree.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsTreeViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsTreeViewHoverResizing == TRUE )
						g_bIsTreeViewHoverResizing = FALSE ;
				}

				if( g_bIsTreeViewResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolTreeWidth = g_pnodeCurrentTabPage->rectSymbolTree.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolTreeWidth < TREEVIEW_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolTreeWidth = TREEVIEW_WIDTH_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultEdit )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultEdit.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultEdit.right && g_pnodeCurrentTabPage->rectScintilla.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultEdit.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultEditHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
						g_bIsSqlQueryResultEditHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSqlQueryResultEditHeight = g_pnodeCurrentTabPage->rectQueryResultEdit.bottom - y - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSqlQueryResultEditHeight < SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
						g_stEditUltraMainConfig.nSqlQueryResultEditHeight = SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN ;

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultTable )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultListView.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultListView.right && g_pnodeCurrentTabPage->rectQueryResultEdit.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultListView.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultListViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
						g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					int	nListViewHeight = g_pnodeCurrentTabPage->rectQueryResultListView.bottom - y - SPLIT_WIDTH/2 ;
					if( nListViewHeight >= SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
					{
						g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = nListViewHeight ;
					}

					UpdateAllWindows( g_hwndMainWindow );
				}
			}

			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) - g_nToolBarHeight ;
				RECT rectClient ;
				int nTabPageIndex ;

				::GetClientRect( g_hwndMainWindow , & rectClient );

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage , rectFileTreeBar ;
				for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
				{
					int	x1 , x2 ;

					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					x1 = rectTabPage.left ;
					x2 = rectTabPage.right;
					if( g_bIsFileTreeBarShow == TRUE )
					{
						::GetClientRect( g_hwndFileTreeBar , & rectFileTreeBar );
						x1 += rectFileTreeBar.right + SPLIT_WIDTH ;
						x2 += rectFileTreeBar.right + SPLIT_WIDTH ;
					}
					if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom && x < rectClient.right - 50 )
					{
						g_pnodeCloseButtonTabPage = GetTabPageByIndex(nTabPageIndex) ;
						::SetWindowPos( g_hwndTabCloseButton , HWND_TOP , x2-TABCLOSEBUTTON_WIDTH+13 , rectTabPage.top+4+g_nToolBarHeight , TABCLOSEBUTTON_WIDTH , TABCLOSEBUTTON_HEIGHT , SWP_SHOWWINDOW );
						::ShowWindow( g_hwndTabCloseButton , SW_SHOW );
						::InvalidateRect( g_hwndTabCloseButton , NULL , TRUE );
						::UpdateWindow( g_hwndTabCloseButton );
						break;
					}
				}
				if( nTabPageIndex >= nTabPagesCount )
				{
					g_pnodeCloseButtonTabPage = NULL ;
					::SetWindowPos( g_hwndTabCloseButton , HWND_BOTTOM , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
					::ShowWindow( g_hwndTabCloseButton , SW_HIDE );
					::InvalidateRect( g_hwndTabCloseButton , NULL , TRUE );
					::UpdateWindow( g_hwndTabCloseButton );
				}
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		break;
	case WM_NOTIFY:
		lpnmhdr = (NMHDR*)lParam ;
		lpnotify = (SCNotification*)lParam ;
		lpnmtv = (NMTREEVIEW*)lParam ;
		pTTT = (TOOLTIPTEXT *)lParam;

		switch (lpnmhdr->code)
		{
		case SCN_CHARADDED:
			OnCharAdded( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case TCN_SELCHANGE:
			OnSelectChange();
			break;
		case SCN_SAVEPOINTREACHED:
			OnSavePointReached( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_SAVEPOINTLEFT:
			OnSavePointLeft( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_MARGINCLICK:
			OnMarginClick( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case SCN_UPDATEUI:
			if( ( lpnotify->updated & SC_UPDATE_SELECTION ) )
			{
				OnEditorTextSelected( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
				UpdateStatusBarSelectionInfo();
			}
			break;
		case TVN_ITEMEXPANDING:
			if( lpnmhdr->hwndFrom == g_hwndFileTree )
			{
				OnFileTreeNodeExpanding( lpnmtv );
			}
			break;
		case NM_DBLCLK:
			if( lpnmhdr->hwndFrom == g_hwndFileTree )
			{
				nret = OnFileTreeNodeDbClick() ;
				if( nret == 1 )
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
			else if( g_pnodeCurrentTabPage && lpnmhdr->hwndFrom == g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
				return 0;
			}
			break;
		case TTN_NEEDTEXT:
			pnodeTabPage = GetTabPageByIndex( (int)(pTTT->hdr.idFrom) ) ;
			if( pnodeTabPage )
			{
				memset( pTTT->szText , 0x00 , sizeof(pTTT->szText) );
				if( (int)(pTTT->hdr.idFrom) <= 8 )
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s - (Alt+%d)" , pnodeTabPage->acPathFilename , (int)(pTTT->hdr.idFrom)+1 );
				else
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s" , pnodeTabPage->acPathFilename );
			}
			break;
		}

		break;
	/*
	case WM_DRAWITEM:
		{
			LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
			if( pDIS->hwndItem == g_hwndTabPages )
			{
			}
		}
		break;
	case WM_MEASUREITEM:
		{
			LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
			if( pDIS->hwndItem == g_hwndTabPages )
			{
			}
		}
		break;
	*/
	case WM_COPYDATA:
		{
			COPYDATASTRUCT	*cpd ;
			cpd = (COPYDATASTRUCT*)lParam ;
			char acPathFilename[MAX_PATH] = "" ;
			strncpy( acPathFilename , (char*)(cpd->lpData) , MIN(sizeof(acPathFilename)-1,cpd->cbData) );
			
			if( acPathFilename[strlen(acPathFilename)-1] != '\\' )
				OpenFilesDirectly( acPathFilename );
			else
				LocateDirectory( acPathFilename );
		}

		{
			HWND hForeWnd = NULL; 
			DWORD dwForeID; 
			DWORD dwCurID; 

			hForeWnd = ::GetForegroundWindow(); 
			dwCurID = ::GetCurrentThreadId(); 
			dwForeID = ::GetWindowThreadProcessId( hForeWnd, NULL ); 
			::AttachThreadInput( dwCurID, dwForeID, TRUE); 
			::SetWindowPos( g_hwndMainWindow, HWND_TOP, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE ); 
			::SetForegroundWindow( g_hwndMainWindow ); 
			::AttachThreadInput( dwCurID, dwForeID, FALSE);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint( hWnd, & ps );
		/*
		RECT rect ;
		::GetClientRect( hWnd , & rect );
		::FillRect( hdc , & rect , g_brushCommonControlDarkColor );
		*/
		EndPaint( hWnd, & ps);
		break;
	case WM_CTLCOLORBTN:
		if( (HWND)lParam == g_hwndTabCloseButton && g_pnodeCloseButtonTabPage )
		{
			HWND btn = (HWND)lParam ;
			HDC hdc = (HDC)wParam ;
			RECT rect;
			::GetClientRect( btn , & rect );
			::SetTextColor( hdc , ::GetSysColor(COLOR_BTNFACE) );
			::SetBkColor( hdc , ::GetSysColor(COLOR_HIGHLIGHT) );
			::SetBkMode( hdc , TRANSPARENT);
			::DrawText( hdc , "X" , 1 , & rect , DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			return (LRESULT)g_brushTabCloseButton;
		}
	case WM_CTLCOLORLISTBOX:
	case WM_CTLCOLOREDIT:
		{
			HDC hdc = (HDC)wParam ;
			::SetTextColor( hdc , g_pstWindowTheme->stStyleTheme.text.color );
			::SetBkColor( hdc , g_pstWindowTheme->stStyleTheme.text.bgcolor );
			::SetBkMode( hdc , TRANSPARENT);
			return (LRESULT)g_brushCommonControlDarkColor;
		}
		break;
	case WM_ACTIVATE:
		if( g_pnodeCurrentTabPage )
		{
			if( hWnd == g_hwndMainWindow )
			{
				OnSelectChange();
			}
		}
		break;
	case WM_LBUTTONDOWN:
		if( g_bIsFileTreeBarHoverResizing == TRUE )
			g_bIsFileTreeBarResizing = TRUE ;

		if( g_bIsFunctionListHoverResizing == TRUE )
			g_bIsFunctionListResizing = TRUE ;

		if( g_bIsTreeViewHoverResizing == TRUE )
			g_bIsTreeViewResizing = TRUE ;

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
			g_bIsSqlQueryResultEditResizing = TRUE ;

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
			g_bIsSqlQueryResultListViewResizing = TRUE ;

		break;
	case WM_LBUTTONUP:
		if( g_bIsFileTreeBarResizing == TRUE )
		{
			g_bIsFileTreeBarResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsFunctionListResizing == TRUE )
		{
			g_bIsFunctionListResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsTreeViewResizing == TRUE )
		{
			g_bIsTreeViewResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultEditResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultListViewResizing = FALSE ;
			UpdateAllWindows( g_hwndMainWindow );
		}

		break;
	case WM_DROPFILES:
		if( wParam )
		{
			OnOpenDropFile( (HDROP)wParam );
		}
		break;
	case WM_CLOSE:
		if( hWnd == g_hwndMainWindow )
		{
			nret = CheckAllFilesSave() ;
			if( nret )
				break;
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		CenterWindow( hDlg , g_hwndMainWindow );
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
#if 0
char buf[1024];
memset(buf,0x00,sizeof(buf));
g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_STYLEGETFONT , STYLE_DEFAULT , (sptr_t)buf );
InfoBox(buf);
#endif
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
