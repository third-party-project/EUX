-- EDITULTRA BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : MySQL
--  DBHOST : localhost
--  DBPORT : 3306
--  DBUSER : calvin
--  DBPASS : calvin
--  DBNAME : calvindb
-- EDITULTRA END DATABASE CONNECTION CONFIG

DROP TABLE IF EXISTS `test_editultra`;
CREATE TABLE `test_editultra`  (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `trans_jnls_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trans_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `trans_desc` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `response_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `trans_amt` decimal(12, 2) NULL DEFAULT NULL,
  `trans_timestamp` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `trans_jnls_no_UNIQUE`(`trans_jnls_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

SELECT * FROM test_editultra;

SELECT * FROM test_editultra;SELECT * FROM test_editultra;

SELECT * FROM pm_config WHERE key="PASSWORD_INVALID_COUNT_MAX";

select *
from jn_trans_list
where trans_jnls_no='2020052312345678';

select trans_jnls_no,trans_code FROM test_editultra where id=2;

INSERT INTO test_editultra VALUES ( 1 , '2020062100000001' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 2 , '2020062100000002' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 3 , '2020062100000003' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 4 , '2020062100000004' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 5 , '2020062100000005' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 6 , '2020062100000006' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 7 , '2020062100000007' , 'APPC0001' , NULL , NULL , NULL , NULL );

DELETE FROM test_editultra;

SELECT * FROM test_editultr;
